const path = require('path');
const webpack = require('webpack');
const TerserPlugin = require("terser-webpack-plugin");

module.exports = {
    mode: 'development',
    entry: {
        'yantra': path.resolve(__dirname, 'src', 'index.ts'),
        'yantra.min': path.resolve(__dirname, 'src', 'index.ts')
    },
    output: {
        path: path.resolve(__dirname, 'dist', 'esm'),
        filename: '[name].js',
        library: {
            name: 'Yantra',
            type: 'umd',
            export: 'default',
            umdNamedDefine: true
        },
        globalObject: 'this'
    },
    devtool: 'source-map',
    optimization: {
        minimize: true,
        minimizer: [
            new TerserPlugin({
                include: /\.min\.js(\?.*)?$/i
            })
        ]
    },
    module: {
        rules: [
            { test: /\.js$/, use:[{loader: "babel-loader"}], exclude: /node_modules/ },
            { test: /\.ts$/, use:[{
                loader: "ts-loader",
                options: {
                    configFile: "tsconfig.distribute.json"
                }
            }], exclude: /node_modules/ },
            { test: /\.css$/, use:[{loader: "css-loader"}] },
            { test: /\.scss$/, use:[{loader: "css-loader"}, {loader: "sass-loader"}] },
            { test: /\.(png|jpg|gif|svg)$/, use:[{loader: "file-loader"}] },
            { test: /\.(ttf|woff|woff2|eot)$/, use:[{loader: "url-loader"}] }
        ]
    },
    resolve: {
        alias: {
            "@lib": path.join(__dirname, "src", "lib")
        },
        extensions: [".js", ".ts"],
    }
};