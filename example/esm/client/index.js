import Engine from "@yantra";

class App{
	constructor(){
		this.engine = new Engine({
			canvas: "rendercanvas"
		});
	}

	init(){
		console.log("app initialized");
	}
}

document.addEventListener("DOMContentLoaded", ()=>{
	const mainapp = new App();
	mainapp.init();
});