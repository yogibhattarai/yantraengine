import Express from "express";
import fs from "fs";
import https from "https";
import http from "http";
import path from "path";

import "regenerator-runtime/runtime";

import __APIENV__ from "./env.json";

import webpack from "webpack";
import webpackDevMiddleware from "webpack-dev-middleware";
import webpackConfig from "./webpack.config.js";

const compiler = webpack(webpackConfig);

const app = Express();
const port = isNaN(__APIENV__.serviceport)
	? parseInt(__APIENV__.serviceport)
	: __APIENV__.serviceport;

app.use(
	webpackDevMiddleware(compiler, {
		publicPath: "",
		serverSideRender: true,
	})
);

/*** Only needed to build library ***/
app.use(
	"/shaders",
	Express.static(path.resolve(__dirname, "..", "..", "src", "shaders"))
);
app.use("/data", Express.static(path.resolve(__dirname, "..", "..", "src", "data")));
/************************************/

//app.use("/", Express.static(path.resolve(__dirname, "dist")));
app.use(
	"/gameassets",
	Express.static(path.resolve(__dirname, "..", "..", "gameassets"))
);
app.use(handleRender);

function handleRender(req, res) {
	res.sendFile(path.resolve(__dirname, "client", "index.html"));
}

let _httpserver;

if (
	__APIENV__.SSL.enabled &&
	fs.existsSync(__APIENV__.SSL.keyfilepath) &&
	fs.existsSync(__APIENV__.SSL.certfilepath)
) {
	_httpserver = https.createServer(
		{
			key: fs.readFileSync(__APIENV__.SSL.keyfilepath),
			cert: fs.readFileSync(__APIENV__.SSL.certfilepath),
		},
		app
	);
} else {
	_httpserver = http.Server(app);
}

_httpserver.listen(port, (err) => {
	if (err) {
		console.log("App Server could not start");
	}
	console.log(
		"Application started @ port " +
			port +
			" on Cluster Process: " +
			process.pid +
			" in " +
			(__APIENV__.SSL.enabled ? "SSL" : "Normal") +
			" mode."
	);
});
