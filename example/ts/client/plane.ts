import { GameObject, Material, Vector, Color, DrawModes, uuidv4 } from "../../../dist/ts/index.js";

interface IPlaneProps{
	id?: string;
	segment?: number;
	smoothLighting?: boolean;
	drawMode?: DrawModes;
	disableCreation?: boolean;
}

class Plane {

	public id: string;

	protected _gameObject: GameObject;

	private planeProps: IPlaneProps;

	constructor(planeProps?: IPlaneProps){
		this.setupDefaults(planeProps);

		this._gameObject = new GameObject({
			drawMode: this.planeProps.drawMode ?? DrawModes.TRIANGLES
		});

		if(!this.planeProps?.disableCreation) this.create();
	}

	private setupDefaults(planeProps?: IPlaneProps): void{
		const newUUID = uuidv4();
		const _planeProps = planeProps ?? {id: newUUID};
		if(!_planeProps.id) _planeProps.id = newUUID;
		this.id = _planeProps.id;

		// max num of segment == 255. This is due to max number of rederable vertices/triangles per draw call. Will have to look into this in the future
		if(!_planeProps.segment) _planeProps.segment = 128;
		else if(_planeProps.segment > 255) _planeProps.segment = 255;

		if(!_planeProps.smoothLighting) _planeProps.smoothLighting = false;
		if(!_planeProps.drawMode) _planeProps.drawMode = DrawModes.TRIANGLES;
		if(!_planeProps.disableCreation) _planeProps.disableCreation = false;

		this.planeProps = _planeProps;
	}
	public updateProps(planeProps?: IPlaneProps): void{
		if(planeProps?.segment) this.planeProps.segment = planeProps.segment;
		if(this.planeProps?.segment && this.planeProps?.segment > 255) this.planeProps.segment = 255;

		if(planeProps?.smoothLighting) this.planeProps.smoothLighting = planeProps?.smoothLighting;
		if(planeProps?.drawMode) this.planeProps.drawMode = planeProps?.drawMode ?? DrawModes.TRIANGLES;
	}

	public get position(): Vector{
		return this._gameObject.position;
	}
	public set position(_position: Vector){
		this._gameObject.position = _position.clone();
	}

	public get rotation(): Vector{
		return this._gameObject.rotation;
	}
	public set rotation(_rotation: Vector){
		this._gameObject.rotation = _rotation.clone();
	}

	public get scale(): Vector{
		return this._gameObject.scale;
	}
	public set scale(_scale: Vector){
		this._gameObject.scale = _scale.clone();
	}

	public get material(): Material{
		return this._gameObject.material;
	}
	public set material(_material: Material){
		this._gameObject.material = _material;
	}

	public get gameObject(): GameObject{
		return this._gameObject;
	}

	private generatePlaneMesh(segment: number, smoothLighting: boolean = false): IPlaneGeometryProps{
		const vertices: Vector[] = [];
		const vertexColors: Color[] = [];
		const texCoords: Vector[] = [];

		for(let h=0; h <= segment; h++){
			for(let w=0; w <= segment; w++){

				const x = w * (1/segment);
				const y = h * (1/segment);

				vertices.push(new Vector(x, y, 0.0));
				vertexColors.push(
					new Color(
						1.0 - (((w/segment) * 0.5) + ((h/segment) * 0.5)),
						(h/segment) - (w/segment),
						((w/segment) * 0.5) + ((h/segment) * 0.5)
					)
				);
			}
		}

		const indices: number[] = this.generateIndices(segment);
		const normals: Vector[] = this.calculateNormals(vertices, indices, smoothLighting);

		return {
			vertices,
			vertexColors,
			normals,
			textureCoordinates: texCoords,
			indices
		};
	}

	private generateIndices(segment: number): number[]{
		const indices: number[] = [];

		for(let y=0; y<segment; y++){
			for(let x=0; x<segment; x++){

				const indexposition01 = (y * (segment + 1)) + x;
				const indexposition02 = ((y + 1) * (segment + 1)) + x;
				const indexposition03 = (y * (segment + 1)) + (x + 1);

				indices.push(indexposition01, indexposition02, indexposition03);

				const indexposition04 = (y * (segment + 1)) + (x + 1);
				const indexposition05 = ((y + 1) * (segment + 1)) + x;
				const indexposition06 = ((y + 1) * (segment + 1)) + (x + 1);

				indices.push(indexposition04, indexposition05, indexposition06);
			}
		}

		return indices;
	}

	private calculateNormals(vertices: Vector[], indices: number[], smoothLighting: boolean = false): Vector[]{
		
		const vertices_arr: number[] = [];
		for(let vect=0; vect<vertices.length; vect++){
			vertices_arr.push(
				vertices[vect].x,
				vertices[vect].y,
				vertices[vect].z
			);
		}

		const _normals: Vector[] = [];
		for(let ind=0; ind<indices.length; ind++){
			// Using try catch is not ideal way of calculating normals. I need to further investigate
			try{
				//per face
				if(ind < indices.length - 20 && smoothLighting){
					const u_uv_01 = vertices[indices[ind + 2]].substract(vertices[indices[ind]]);
					const v_uv_01 = vertices[indices[ind + 1]].substract(vertices[indices[ind]]);
					const normal_01 = u_uv_01.cross(v_uv_01);

					const u_uv_02 = vertices[indices[ind + 11]].substract(vertices[indices[ind + 9]]);
					const v_uv_02 = vertices[indices[ind + 10]].substract(vertices[indices[ind + 9]]);
					const normal_02 = u_uv_02.cross(v_uv_02);

					const u_uv_03 = vertices[indices[ind + 20]].substract(vertices[indices[ind + 18]]);
					const v_uv_03 = vertices[indices[ind + 19]].substract(vertices[indices[ind + 18]]);
					const normal_03 = u_uv_03.cross(v_uv_03);

					_normals[indices[ind]] = new Vector(
						(normal_01.normalize.x + normal_02.normalize.x + normal_03.normalize.x) / 3,
						(normal_01.normalize.y + normal_02.normalize.y + normal_03.normalize.y) / 3,
						(normal_01.normalize.z + normal_02.normalize.z + normal_03.normalize.z) / 3
					);
				}
				//per vertex
				else if(ind < indices.length - 3/* && indices[ind + 2] && vertices[indices[ind + 2]]*/) {
					const u_uv = vertices[indices[ind + 2]].substract(vertices[indices[ind]]);
					const v_uv = vertices[indices[ind + 1]].substract(vertices[indices[ind]]);
					const normal = u_uv.cross(v_uv);                // dot product to find planar slope
					const normalizedvector = normal.normalize;

					_normals[indices[ind]] = new Vector(normalizedvector.x, normalizedvector.y, normalizedvector.z);
				}
			}
			catch(err){
				console.error(err);
			}
		}

		const normals: Vector[] = [];
		for(let norm=0; norm<_normals.length; norm++){
			normals.push(new Vector(_normals[norm].x, _normals[norm].y, _normals[norm].z));
		}
		normals.push(new Vector(_normals[normals.length - 1].x, _normals[normals.length - 1].y, _normals[normals.length - 1].z));

		return normals;
	}

	private create(): void {

		const {
			vertices,
			vertexColors,
			normals,
			textureCoordinates,
			indices
		} = this.generatePlaneMesh(this.planeProps?.segment ?? 128, this.planeProps?.smoothLighting ?? false);

		this._gameObject.geometry.vertex = vertices;
		this._gameObject.geometry.vertexColor = vertexColors;
		this._gameObject.geometry.normal = normals;
		this._gameObject.geometry.uv = textureCoordinates;
		this._gameObject.geometry.index = indices;
	}
}

interface IPlaneGeometryProps {
	vertices: Vector[];
	vertexColors: Color[];
	normals: Vector[];
	textureCoordinates: Vector[];
	indices: number[];
};

export {
	Plane,
	IPlaneGeometryProps
};