import Engine from "../../../dist/ts/index.js";
import MeshScene from "./meshscene";
import TerrainScene from "./terrainscene";
import Stats from "stats.js";

class App{

	private engine: Engine;

	// private meshScene: MeshScene;
	private terrainScene: TerrainScene;

	private stats: Stats = new Stats();

	constructor(){
		this.engine = new Engine({
			canvas: "rendercanvas",
			/*
			fps: 30,
			*/
			fullScreen: true
		});

		// this.meshScene = new MeshScene(this.engine.canvas);
		this.terrainScene = new TerrainScene(this.engine.canvas);

		this.stats.showPanel(0); // 0: fps, 1: ms, 2: mb, 3+: custom
		document.body.appendChild(this.stats.dom);
	}

	public async init(): Promise<void>{
		console.log("app initialized", this.engine);

		try{
			// const _scene = await this.meshScene.create();
			const _scene = this.terrainScene.create();// await this.terrainScene.create();

			/***** Single Draw Call *****/
			// await this.engine.compileScene(mesh_scene);
			// this.engine.render(mesh_scene);
			/****************************/

			/****** Multi Draw Call ******/
			await this.engine.addScene(_scene);
			this.engine.run(
				()=>{
					// console.log("before Render");
					this.stats.begin();
					// this.meshScene.update();
					this.terrainScene.update();
				},
				()=>{
					// console.log("after render");
					this.stats.end();
				}
			);
			/******************************/
		}catch(err){
			console.error(err);
		}
	}
}

document.addEventListener("DOMContentLoaded", ()=>{
	const mainapp = new App();
	mainapp.init()
		.then(_ => {})
		.catch(error => console.error(error));
});