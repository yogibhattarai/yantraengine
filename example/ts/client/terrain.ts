import { Material, DrawModes, Vector, Color, toRadian, generateSeed } from "../../../dist/ts/index.js";
import { Plane } from "./plane";
import { TerrainGenerator, ITerrainGeneratorProps, INoiseMap } from "./terrain.generator";

interface IMapProps{
	noiseScale?: number;
	octaves?: number;
	persistence?: number;
	lacunarity?: number;
}

interface ITerrainProps{
	size?: number;
	segments?: number;
	displacementMultiplier?: number;
	flatWater?: boolean;
	offset?: Vector;
	mapProps?: IMapProps;
	drawMode?: DrawModes;
	seed?: string;
}

class Terrain extends Plane{

	private terrainGenerator: TerrainGenerator;

	private terrainProps: ITerrainProps;

	private terrainNoiseMap: INoiseMap;

	constructor(terrainProps?: ITerrainProps){
		super({
			segment: terrainProps?.segments ?? 128,
			drawMode: terrainProps?.drawMode ?? DrawModes.TRIANGLES
		});

		this.setupDefaultProps(terrainProps);
		this.initTerrainGenerator();
		this._create();
	}

	private setupDefaultProps(terrainProps?: ITerrainProps): void{
		const defaultTerrainProps: ITerrainProps = (this.terrainProps ? {...this.terrainProps, ...terrainProps} : terrainProps) ?? {};

		if(!defaultTerrainProps?.size || defaultTerrainProps?.size<0) defaultTerrainProps.size = 1;

		if(!defaultTerrainProps?.segments || defaultTerrainProps?.segments<0) defaultTerrainProps.segments = 128;
		else if(defaultTerrainProps?.segments > 255) defaultTerrainProps.segments = 255;

		if(!defaultTerrainProps?.displacementMultiplier || defaultTerrainProps?.displacementMultiplier<0) defaultTerrainProps.displacementMultiplier = 0.1;
		if(typeof defaultTerrainProps?.flatWater !== "boolean") defaultTerrainProps.flatWater = false;
		if(!defaultTerrainProps.offset) defaultTerrainProps.offset = new Vector();

		if(!defaultTerrainProps?.mapProps) defaultTerrainProps.mapProps = {};
		
		if(!defaultTerrainProps?.mapProps?.noiseScale || defaultTerrainProps?.mapProps?.noiseScale<0) defaultTerrainProps.mapProps.noiseScale = 50;
		if(!defaultTerrainProps?.mapProps?.octaves || defaultTerrainProps?.mapProps?.octaves<0) defaultTerrainProps.mapProps.octaves = 4;
		if(!defaultTerrainProps?.mapProps?.persistence || defaultTerrainProps?.mapProps?.persistence<0) defaultTerrainProps.mapProps.persistence = 0.5;
		if(!defaultTerrainProps?.mapProps?.lacunarity || defaultTerrainProps?.mapProps?.lacunarity<0) defaultTerrainProps.mapProps.lacunarity = 2;

		if(!defaultTerrainProps.drawMode) defaultTerrainProps.drawMode = DrawModes.TRIANGLES;
		const generatedSeed = generateSeed();
		if(!defaultTerrainProps.seed) defaultTerrainProps.seed = `${generatedSeed}`;

		this.terrainProps = defaultTerrainProps;
	}
	public updateTerrainProps(terrainProps?: ITerrainProps): void{
		if(!terrainProps) terrainProps = {};

		const generatedSeed = generateSeed();
		if(!terrainProps?.seed) terrainProps.seed = `${generatedSeed}`;
		this.terrainGenerator.updateProps({seed: terrainProps.seed});

		this.setupDefaultProps(terrainProps);

		this.updateProps({
			segment: this.terrainProps?.segments ?? 128
		});

		this.initTerrainGenerator(true);
		this._create();
		this._gameObject.updateShaderGeometryAttributes();
	}

	private initTerrainGenerator(update?: boolean): void{
		const generatedSeed = generateSeed();
		const terrainGenProps: ITerrainGeneratorProps = {
			width: (this.terrainProps?.segments ?? 128) + 1,
			height: (this.terrainProps?.segments ?? 128) + 1,
			seed: this.terrainProps?.seed ?? `${generatedSeed}`,
			offset: this.terrainProps?.offset ?? new Vector(),
			noiseScale: (this.terrainProps?.mapProps?.noiseScale ?? 50),
			octaves: (this.terrainProps?.mapProps?.octaves ?? 4),
			persistence: (this.terrainProps?.mapProps?.persistence ?? 0.5),
			lacunarity: (this.terrainProps?.mapProps?.lacunarity ?? 2)
		};

		if(update && this.terrainGenerator) this.terrainGenerator.updateProps(terrainGenProps);
		else this.terrainGenerator = new TerrainGenerator(terrainGenProps);
	}

	private generateTerrainData(): ITerrainGeometryProps{
		const vertices: Vector[] = [];
		const vertexColors: Color[] = [];

		try{
			for(let h=0; h<=(this.terrainProps?.segments ?? 128); h++){
				for(let w=0; w<=(this.terrainProps?.segments ?? 128); w++){

					const x = w * ((this.terrainProps.size ?? 1)/(this.terrainProps?.segments ?? 128));
					const y = h * ((this.terrainProps.size ?? 1)/(this.terrainProps?.segments ?? 128));

					const currentheight = (this.terrainNoiseMap.heights[w][h] < -0.2 && this.terrainProps.flatWater ? -0.2 : this.terrainNoiseMap.heights[w][h]) * (this.terrainProps.displacementMultiplier ?? 0.1);

					vertices.push(new Vector(x, y, currentheight));
					vertexColors.push(
						new Color(
							this.terrainNoiseMap.colors[w][h].r,
							this.terrainNoiseMap.colors[w][h].g,
							this.terrainNoiseMap.colors[w][h].b
						)
					);
				}
			}
		}
		catch(err){console.error(err);}

		return {
			vertices,
			vertexColors
		};
	}

	private _create(): void{
		this.terrainNoiseMap = this.terrainGenerator.generate();
		const {
			vertices,
			vertexColors
		} = this.generateTerrainData();

		this._gameObject.geometry.vertex = vertices;
		this._gameObject.geometry.vertexColor = vertexColors;

		this._gameObject.drawMode = this.terrainProps.drawMode ?? DrawModes.TRIANGLES;
	}
}

interface ITerrainGeometryProps {
	vertices: Vector[];
	vertexColors: Color[];
};

export {
	Terrain,
	ITerrainProps,
	IMapProps
};