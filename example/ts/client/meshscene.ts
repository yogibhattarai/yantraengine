import { Scene, PerspectiveCamera, OrthographicCamera, Material, Vector, Color, toRadian, randomInt, randomFloat, DirectionalLight, AmbientLight, DrawModes } from "../../../dist/ts/index.js";
import { Cube } from "./cube";
import { Sphere } from "./sphere";
import { Torus } from "./torus";
import { Plane } from "./plane";
import { OrbitCamera } from "./orbitcamera";
import { FPSCamera } from "./fpscamera";

export default class MeshScene{
	
	private canvas: HTMLCanvasElement;

	public scene: Scene;
	private camera: PerspectiveCamera | OrthographicCamera;

	private orbitcam: OrbitCamera;
	// private fpscam: FPSCamera;

	private gameObjects: (Cube | Sphere | Torus | Plane)[]  = [];
	private yRotations: number[] = [];
	private rotationDirection: number[] = [];
	private rotationSpeeds: number[] = [];

	// private torus: Torus;
	// private torusYRot: number = 0;

	constructor(canvas: HTMLCanvasElement){
		this.canvas = canvas;
	}

	public async create(): Promise<Scene>{
		this.scene = new Scene();

		this.camera = new PerspectiveCamera({
			fov: 75,
			aspect: this.canvas.width/this.canvas.height,
			near: 0.1,
			far: 1000
		});
		// this.camera = new OrthographicCamera();
		this.scene.add(this.camera);

		this.orbitcam = new OrbitCamera(this.camera, this.canvas, {lookAt: new Vector(0, 0, 0), radius: 5, dampingFactor: 0.25, zoomMultiplier: 0.5, limits: {maxRadius: 20, minRadius: 1}});
		// this.fpscam = new FPSCamera(this.camera, this.canvas);

		for(let meshType=0; meshType<4; meshType++){
			const randNum = randomInt(2);
			const rotationDirection = randNum === 1 ? 1 : -1;
			const rotSpeed = randomFloat(1.0, 0.1);

			const yRotation = randomInt(360);
			const _rotation = new Vector(meshType === 2 ? toRadian(-90) : 0, toRadian(yRotation), 0);
			const _position = new Vector(
				meshType - 2,
				0,
				0
			);
			const _scaleScalar = randomFloat(1, 0.5);
			const _scale = new Vector(_scaleScalar, _scaleScalar, _scaleScalar);

			let mesh: Cube | Sphere | Torus | Plane = new Cube();

			if(meshType === 0) mesh = new Cube();
			else if(meshType === 1) mesh = new Sphere();
			else if(meshType === 2) mesh = new Torus();
			else if(meshType === 3) mesh = new Plane();

			mesh.position = _position;
			mesh.rotation = _rotation;
			mesh.scale = _scale;
			this.gameObjects.push(mesh);

			this.yRotations.push(yRotation);
			this.rotationDirection.push(rotationDirection);
			this.rotationSpeeds.push(rotSpeed);

			const materialType: number = 0;

			let gameObjectMaterial: Material;
			if(materialType === 0){
				const _color = new Color(randomInt(256, 50), randomInt(256, 50), randomInt(256, 50));
				gameObjectMaterial = new Material({
					color: _color
					/*
					unlit: true,
					debug: true,
					flat: true
					*/
				});
			}
			else{
				gameObjectMaterial = new Material({
					texture: "/gameassets/textures/texturemap_1024.png"
				});
			}

			mesh.material = gameObjectMaterial;
			
			this.scene.add(mesh.gameObject);
		}

		// const ambientLight = new AmbientLight({
		// 	color: new Color(44, 58, 212)
		// });
		// this.scene.add(ambientLight);

		const directionalLight = new DirectionalLight({
			direction: new Vector(0.85, 0.8, 0.75),
			color: new Color(1, 1, 1)
		});
		this.scene.add(directionalLight);

		return Promise.resolve(this.scene);
	}

	public update(): void{
		this.gameObjects.forEach((_gameObject, c_index)=>{
			this.yRotations[c_index] += this.rotationDirection[c_index] * this.rotationSpeeds[c_index];
			if(this.yRotations[c_index] > 360) this.yRotations[c_index] = 0;

			const _rotation = new Vector(
				_gameObject instanceof Torus ? toRadian(-90) : 0,
				_gameObject instanceof Torus ? 0 : toRadian(this.yRotations[c_index]),
				_gameObject instanceof Torus ? toRadian(this.yRotations[c_index]) : 0
			);

			_gameObject.gameObject.rotation = _rotation;
		});

		this.orbitcam.update();
		// this.fpscam.update();
	}
}