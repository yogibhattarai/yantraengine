import { GameObject, Material, Vector, Color, DrawModes, FaceDirection, uuidv4 } from "../../../dist/ts/index.js";

interface ITorusProps{
	id?: string;
	donutRadius?: number;
	tubeRadius?: number;
	verticalSegments?: number;
	horizontalSegments?: number;
	faceDirection?: FaceDirection;
	drawMode?: DrawModes;
}

class Torus {

	public id: string;

	private _gameObject: GameObject;

	private torusProps: ITorusProps;

	constructor(torusProps?: ITorusProps){
		this.setupDefaults(torusProps);

		this._gameObject = new GameObject({
			faceDirection: this.torusProps.faceDirection ?? FaceDirection.CCW,
			drawMode: this.torusProps.drawMode ?? DrawModes.TRIANGLE_STRIP
		});

		this.create();
	}

	private setupDefaults(torusProps?: ITorusProps): void{
		const newUUID = uuidv4();
		const _torusProps = torusProps ?? {id: newUUID};
		if(!_torusProps.id) _torusProps.id = newUUID;
		this.id = _torusProps.id;

		if(!_torusProps.donutRadius) _torusProps.donutRadius = 1;						// this is R
		if(!_torusProps.tubeRadius) _torusProps.tubeRadius = 0.25;						// this is r
		if(!_torusProps.verticalSegments) _torusProps.verticalSegments = 16;			// this is v
		if(_torusProps.verticalSegments < 4) _torusProps.verticalSegments = 4;
		else if(_torusProps.verticalSegments > 32) _torusProps.verticalSegments = 32;
		if(!_torusProps.horizontalSegments) _torusProps.horizontalSegments = 32;		// this is u
		if(_torusProps.horizontalSegments < 8) _torusProps.horizontalSegments = 8;
		else if(_torusProps.horizontalSegments > 64) _torusProps.horizontalSegments = 64;

		if(!_torusProps.faceDirection) _torusProps.faceDirection = FaceDirection.CCW;
		if(!_torusProps.drawMode) _torusProps.drawMode = DrawModes.TRIANGLE_STRIP;

		this.torusProps = _torusProps;
	}

	public get position(): Vector{
		return this._gameObject.position;
	}
	public set position(_position: Vector){
		this._gameObject.position = _position.clone();
	}

	public get rotation(): Vector{
		return this._gameObject.rotation;
	}
	public set rotation(_rotation: Vector){
		this._gameObject.rotation = _rotation.clone();
	}

	public get scale(): Vector{
		return this._gameObject.scale;
	}
	public set scale(_scale: Vector){
		this._gameObject.scale = _scale.clone();
	}

	public get material(): Material{
		return this._gameObject.material;
	}
	public set material(_material: Material){
		this._gameObject.material = _material;
	}

	public get gameObject(): GameObject{
		return this._gameObject;
	}

	private generateTorus(donutRadius: number, tubeRadius: number, verticalSegments: number, horizontalSegments: number): ITorusGeometryProps{

		const vertices: Vector[] = [];
		const vertexColors: Color[] = [];
		const normals: Vector[] = [];
		const texCoords: Vector[] = [];
		const tangents: Vector[] = [];

		const du = 2 * Math.PI / horizontalSegments;
		const dv = 2 * Math.PI / verticalSegments;

		let colorCount = 1;
		let darkness = 1;

		for(let i=0; i<horizontalSegments; i++){

			const u = i * du;

			let v: number = 0;

			colorCount = 1;
			darkness = darkness===1 ? 0.5 : 1;

			for(let j=0; j<=verticalSegments; j++){

				v = (j % verticalSegments) * dv;

				for(let k=0; k<2; k++){

					const uu = u + k * du;

					// compute vertex
					const x = (donutRadius + tubeRadius * Math.cos(v)) * Math.cos(uu);
					const y = (donutRadius + tubeRadius * Math.cos(v)) * Math.sin(uu);
					const z = tubeRadius * Math.sin(v);

					vertices.push(new Vector(x, y, z));

					let vertexColor = new Color(1 * darkness, 0, 0);
					if(colorCount % 3===0) vertexColor = new Color(0, 1 * darkness, 0);
					else if(colorCount % 2===0) vertexColor = new Color(0, 0, 1 * darkness);
					vertexColors.push(vertexColor);
					colorCount++;

					// compute normals
					const nx = Math.cos(v) * Math.cos(uu);
					const ny = Math.cos(v) * Math.sin(uu);
					const nz = Math.sin(v);

					normals.push(new Vector(nx, ny, nz));

					// compute texture coords
					const tx = uu / (2 * Math.PI);
					const ty = v / (2 * Math.PI);

					texCoords.push(new Vector(tx, ty));

					const tangentVector = new Vector(
						-(donutRadius + tubeRadius * Math.cos(v)) * Math.sin(uu),
						(donutRadius + tubeRadius * Math.cos(v)) * Math.cos(uu),
						0.0
					);
					const normalizedTangentVector = tangentVector.normalize;

					tangents.push(new Vector(normalizedTangentVector.x, normalizedTangentVector.y, normalizedTangentVector.z));

				}

				v += dv;
			}
		}

		return {
			vertices,
			vertexColors,
			normals,
			textureCoordinates: texCoords
		};
	}

	private create(): void {
		const {
			vertices,
			vertexColors,
			normals,
			textureCoordinates,
		} = this.generateTorus(
			this.torusProps.donutRadius ?? 1,
			this.torusProps.tubeRadius ?? 0.25,
			this.torusProps.verticalSegments ?? 16,
			this.torusProps.horizontalSegments ?? 32
		);

		this._gameObject.geometry.vertex = vertices;
		this._gameObject.geometry.vertexColor = vertexColors;
		this._gameObject.geometry.normal = normals;
		this._gameObject.geometry.uv = textureCoordinates;
	}
}

interface ITorusGeometryProps {
	vertices: Vector[];
	vertexColors: Color[];
	normals: Vector[];
	textureCoordinates: Vector[];
};

export {
	Torus,
	ITorusGeometryProps
};