import { Scene, PerspectiveCamera, OrthographicCamera, Material, Vector, Color, toRadian, randomInt, randomFloat, DirectionalLight, AmbientLight, DrawModes, generateSeed } from "../../../dist/ts/index.js";
import { Cube } from "./cube";
import { Sphere } from "./sphere";
import { Plane } from "./plane";
import { ITerrainProps, IMapProps, Terrain } from "./terrain";
import { OrbitCamera } from "./orbitcamera";
import { FPSCamera } from "./fpscamera";

import * as dat from "dat.gui";

interface IOffset {
	x: number;
	y: number;
}

interface IGUISettings {
	"Generate Seed": ()=>void;
}

export default class TerrainScene{
	
	private canvas: HTMLCanvasElement;

	public scene: Scene;
	private camera: PerspectiveCamera | OrthographicCamera;

	private orbitcam: OrbitCamera;
	// private fpscam: FPSCamera;

	private terrain: Terrain;
	
	private terrainProps: ITerrainProps;
	private mapProps: IMapProps;

	private datGUI: dat.GUI;
	private guiSettings: IGUISettings;
	private noiseSettings: dat.GUI;
	
	constructor(canvas: HTMLCanvasElement){
		this.canvas = canvas;

		this.mapProps = {
			noiseScale: 50,
			octaves: 4,
			persistence: 0.5,
			lacunarity: 2
		};

		const _seed = generateSeed();
		this.terrainProps = {
			size: 1,
			segments: 128,
			displacementMultiplier: 0.1,
			flatWater: false,
			offset: new Vector(),
			mapProps: this.mapProps,
			drawMode: DrawModes.TRIANGLES,
			seed: `${_seed}`
		};

		this.datGUI = new dat.GUI();
		this.guiSettings = {
			"Generate Seed": (): void=>{
				const _seed = generateSeed();
				this.terrainProps.seed = `${_seed}`;
				this.noiseSettings.__controllers[4].setValue(this.terrainProps.seed);
				this.updateTerrain();
			}
		};

		this.setupDatGUI();
	}

	private setupDatGUI(): void{
		this.noiseSettings = this.datGUI.addFolder("Noise Settings");
		this.noiseSettings.add(this.mapProps, "noiseScale", 20, 500).name("Noise Scale")
			.step(1).onFinishChange(()=>this.updateTerrain(this.terrainProps));
		this.noiseSettings.add(this.mapProps, "octaves", 0, 10).name("Octaves")
			.step(0.1).onFinishChange(()=>this.updateTerrain(this.terrainProps));
		this.noiseSettings.add(this.mapProps, "persistence", 0, 1).name("Persistence")
			.step(0.01).onFinishChange(()=>this.updateTerrain(this.terrainProps));
		this.noiseSettings.add(this.mapProps, "lacunarity", 0, 10).name("Lacunarity")
			.step(0.01).onFinishChange(()=>this.updateTerrain(this.terrainProps));

		this.noiseSettings.add(this.terrainProps, "seed").name("Seed").onFinishChange(()=>this.updateTerrain(this.terrainProps));
		this.noiseSettings.add(this.guiSettings, "Generate Seed");
		this.noiseSettings.open();

		const offsetsfolder = this.datGUI.addFolder("Map Offsets");
		offsetsfolder.add(this.terrainProps.offset as IOffset, "x").step(0.1).onFinishChange(()=>this.updateTerrain(this.terrainProps));
		offsetsfolder.add(this.terrainProps.offset as IOffset, "y").step(0.1).onFinishChange(()=>this.updateTerrain(this.terrainProps));
		offsetsfolder.open();

		const terrainsettingsfolder = this.datGUI.addFolder("Terrain Settings");
		terrainsettingsfolder.add(this.terrainProps, "size", 1, 5).name("Size").step(1).onFinishChange(()=>this.updateTerrain(this.terrainProps));
		terrainsettingsfolder.add(this.terrainProps, "segments", 4, 255).name("Segments").step(1).onFinishChange(()=>this.updateTerrain(this.terrainProps));
		terrainsettingsfolder.add(
			this.terrainProps,
			"drawMode",
			{
				Points: DrawModes.POINTS,
				Lines: DrawModes.LINES,
				Triangles: DrawModes.TRIANGLES
			}
		).name("Draw Mode").onChange(()=>this.updateTerrain(this.terrainProps));
		terrainsettingsfolder.add(this.terrainProps, "displacementMultiplier", 0.1, 1).step(0.05).name("Displacement Multiplier").onFinishChange(()=>this.updateTerrain(this.terrainProps));
		terrainsettingsfolder.add(this.terrainProps, "flatWater").name("Flat Water").onFinishChange(()=>this.updateTerrain(this.terrainProps));
		terrainsettingsfolder.open();
	}

	// public async create(): Promise<Scene>{
	public create(): Scene{
		this.scene = new Scene();

		this.camera = new PerspectiveCamera({
			fov: 75,
			aspect: this.canvas.width/this.canvas.height,
			near: 0.1,
			far: 1000
		});
		// this.camera = new OrthographicCamera();
		this.scene.add(this.camera);

		this.orbitcam = new OrbitCamera(
			this.camera,
			this.canvas,
			{
				lookAt: new Vector(0, 0, 0),
				radius: 0.5,
				dampingFactor: 0.25,
				zoomMultiplier: 0.5,
				limits: {
					maxRadius: 3.5,
					minRadius: 0.5
				},
				theta: 90,
				phi: 35
			}
		);
		// this.fpscam = new FPSCamera(this.camera, this.canvas);

		this.terrain = new Terrain(this.terrainProps);
		const terrainMaterial = new Material({
			/*
			unlit: true,
			*/
			debug: true,
			flat: true,
			pointSize: 3.0
		});
		this.terrain.material = terrainMaterial;
		this.terrain.position = new Vector(-0.5, 0, -0.5);
		this.terrain.rotation = new Vector(toRadian(-90), 0, 0);
		this.scene.add(this.terrain.gameObject);

		// const ambientLight = new AmbientLight({
		// 	color: new Color(44, 58, 212)
		// });
		// this.scene.add(ambientLight);

		const directionalLight = new DirectionalLight({
			direction: new Vector(0.85, 0.8, 0.75),		// new Vector(0.85, 0.8, 0.75),
			color: new Color(1, 1, 1)
		});
		this.scene.add(directionalLight);

		// return Promise.resolve(this.scene);
		return this.scene;
	}

	public update(): void{
		this.orbitcam.update();
		// this.fpscam.update();
	}

	private updateTerrain(terrainProps?: ITerrainProps): void{
		this.terrain.updateTerrainProps(terrainProps);
	}
}