import { PerspectiveCamera, OrthographicCamera, Vector } from "../../../dist/ts/index.js";

interface ICameraLimits {
	maxTheta?: number;
	minTheta?: number;
	maxPhi?: number;
	minPhi?: number;
	maxRadius?: number;
	minRadius?: number;
}

interface IOrbitCameraProps {
	dampingEnabled?: boolean;
	dampingFactor?: number;
	turnRatio?: number;
	zoomMultiplier?: number;
	radius?: number;
	theta?: number;
	phi?: number;
	limits?: ICameraLimits;
	lookAt?: Vector;
}

class OrbitCamera{

	private camera: PerspectiveCamera | OrthographicCamera;
	private canvas: HTMLCanvasElement;

	private mouseDown: boolean = false;
	private mouseDownPosition: Vector = new Vector();
	private mouseDownThetaPhi: Vector = new Vector();

	private xy: Vector = new Vector();
	private txy: Vector = new Vector();
	private cxy: Vector = new Vector();

	private dampingEnabled: boolean = true;
	private dampingFactor: number = 0.1;

	private turnRatio: number = 0.5;
	private zoomMultiplier: number = 1;
	private radius: number = 1;
	private theta: number = 0;
	private phi: number = 0;

	private limits: ICameraLimits;
	private lookAt: Vector = new Vector(0, 0, 0);

	private isInertial: boolean = false;
	private radiusChanged: boolean = false;

	constructor(maincamera: PerspectiveCamera | OrthographicCamera, canvas: HTMLCanvasElement, props: IOrbitCameraProps){
		this.camera = maincamera;
		this.canvas = canvas;

		this.dampingEnabled = props?.dampingEnabled ?? this.dampingEnabled;
		this.dampingFactor = props?.dampingFactor ?? this.dampingFactor;

		this.turnRatio = props?.turnRatio ?? this.turnRatio;
		this.zoomMultiplier = props?.zoomMultiplier ?? this.zoomMultiplier;
		this.radius = props?.radius ?? this.radius;
		this.theta = props?.theta ?? this.theta;
		this.phi = props?.phi ?? this.phi;

		this.limits = {
			maxTheta: 360,
			minTheta: -360,
			maxPhi: 180,
			minPhi: -180,
			maxRadius: 1,
			minRadius: 1
		};
		this.limits = {...this.limits, ...props?.limits};

		this.lookAt = props?.lookAt && props.lookAt instanceof Vector && props.lookAt.z!==undefined ? props.lookAt : this.lookAt;

		this.setupInputListeners();
		this.setCameraLookAt(this.theta, this.phi);
	}

	private getRotatedPosition(theta: number, phi: number): Vector{
		const positionx = this.lookAt.x + (this.radius * Math.sin(theta * Math.PI/360) * Math.cos(phi * Math.PI/360));
		const positiony = this.lookAt.y + (this.radius * Math.sin(phi * Math.PI/360));
		const positionz = (this.lookAt.z ?? 0) + (this.radius * Math.cos(theta * Math.PI/360) * Math.cos(phi * Math.PI/360));

		const _position = new Vector(positionx, positiony, positionz);
		return _position;
	}

	private setCameraLookAt(theta: number, phi: number): void{
		const rotatedposition = this.getRotatedPosition(theta, phi);

		this.camera.position = new Vector(rotatedposition.x, rotatedposition.y, rotatedposition.z);
		this.camera.lookAt = this.lookAt;
	}

	private getMousePosition(event: MouseEvent): Vector{
		const rect = this.canvas.getBoundingClientRect();
		const scaleX = this.canvas.width / rect.width;
		const scaleY = this.canvas.height / rect.height;

		return new Vector((event.clientX - rect.left) * scaleX, (event.clientY - rect.top) * scaleY);
	}

	private applyInertia(current: number, target: number, amount: number): number{
		if(amount==1) return target;

		let distToGo = target - current;
		let delta = current + (distToGo * amount);

		if(Math.abs(distToGo) < 0.01){
			distToGo = 0;
			delta = target;

			this.isInertial = false;
			this.radiusChanged = false;
		}

		return delta;
	}

	private setCameraOnSphere(): void{
		if(this.mouseDownPosition){
			this.cxy = this.xy.clone();

			this.xy.x = this.applyInertia(this.cxy.x, this.txy.x, this.dampingFactor);
			this.xy.y = this.applyInertia(this.cxy.y, this.txy.y, this.dampingFactor);

			this.theta = -1 * ((this.xy.x - this.mouseDownPosition.x) * this.turnRatio) + this.mouseDownThetaPhi.x;
			this.phi = ((this.xy.y - this.mouseDownPosition.y) * this.turnRatio) + this.mouseDownThetaPhi.y;

			this.checkLimits();

			this.setCameraLookAt(this.theta, this.phi);
		}
	}

	private checkLimits(): void{
		const maxTheta = this.limits.maxTheta ?? 360;
		const minTheta = this.limits.minTheta ?? -360;
		const maxPhi = this.limits.maxPhi ?? 180;
		const minPhi = this.limits.minPhi ?? -180;
		const maxRadius = this.limits.maxRadius ?? 1;
		const minRadius = this.limits.minRadius ?? 1;

		this.phi = Math.min(
			maxPhi,
			Math.max(
				(minPhi < 0 ? 0 : minPhi),
				this.phi
			)
		);

		if(this.theta > maxTheta && maxTheta < 360) this.theta = maxTheta;
		else if(this.theta < minTheta  && minTheta > -360) this.theta = minTheta;

		if(this.radius > maxRadius) this.radius = maxRadius;
		else if(this.radius < minRadius) this.radius = minRadius;
	}

	private setupInputListeners(): void{
		this.canvas.addEventListener("mousedown", (event)=>{
			event.preventDefault();

			if(event.buttons===1){
				this.isInertial = false;
				this.mouseDown = true;
				this.mouseDownThetaPhi = new Vector(this.theta, this.phi);
				this.mouseDownPosition = this.getMousePosition(event);
				this.xy = this.mouseDownPosition.clone();
			}
		});
		this.canvas.addEventListener("mousemove", (event)=>{
			event.preventDefault();

			if(this.mouseDown){
				this.isInertial = true;
				this.txy = this.getMousePosition(event);
			}
		});
		window.addEventListener("mouseup", (event)=>{
			this.mouseDown = false;
		});
		this.canvas.addEventListener("mousewheel", (event): void=>{
			const maxRadius = this.limits.maxRadius ?? 1;
			const minRadius = this.limits.minRadius ?? 1;

			if((event as WheelEvent).deltaY<=0 && this.radius>minRadius) this.radius -= 1 * this.zoomMultiplier;
			else if((event as WheelEvent).deltaY>=0 && this.radius<maxRadius) this.radius += 1 * this.zoomMultiplier;

			this.radiusChanged = true;
		});
	}

	private removeInputListeners(): void{
		this.canvas.removeEventListener("mousedown", ()=>{});
		this.canvas.removeEventListener("mouseup", ()=>{});
		this.canvas.removeEventListener("mousewheel", ()=>{});
	}

	public update(): void{

		if((this.dampingEnabled && this.isInertial) || this.radiusChanged) this.setCameraOnSphere();

		// Hack solution. Need revisit sometime in the future
		if(this.theta <= -360) this.theta = 360 - (-360 - this.theta);
		else if(this.theta >= 360) this.theta = -360 + (this.theta - 360);
	}
}

export {
	OrbitCamera,

	ICameraLimits,
	IOrbitCameraProps
};