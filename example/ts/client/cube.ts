import { GameObject, Material, Vector, Color, DrawModes, uuidv4 } from "../../../dist/ts/index.js";

interface ICubeProps{
	id?: string;
	drawMode?: DrawModes;
}

class Cube {

	public id: string;

	private _gameObject: GameObject;

	private cubeProps: ICubeProps;

	constructor(cubeProps?: ICubeProps){
		this.setupDefaults(cubeProps);

		this._gameObject = new GameObject({
			drawMode: this.cubeProps.drawMode ?? DrawModes.TRIANGLES
		});

		this.create();
	}

	private setupDefaults(cubeProps?: ICubeProps): void{
		const newUUID = uuidv4();
		const _cubeProps = cubeProps ?? {id: newUUID};
		if(!_cubeProps.id) _cubeProps.id = newUUID;
		this.id = _cubeProps.id;

		if(!_cubeProps.drawMode) _cubeProps.drawMode = DrawModes.TRIANGLES;

		this.cubeProps = _cubeProps;
	}

	public get position(): Vector{
		return this._gameObject.position;
	}
	public set position(_position: Vector){
		this._gameObject.position = _position.clone();
	}

	public get rotation(): Vector{
		return this._gameObject.rotation;
	}
	public set rotation(_rotation: Vector){
		this._gameObject.rotation = _rotation.clone();
	}

	public get scale(): Vector{
		return this._gameObject.scale;
	}
	public set scale(_scale: Vector){
		this._gameObject.scale = _scale.clone();
	}

	public get material(): Material{
		return this._gameObject.material;
	}
	public set material(_material: Material){
		this._gameObject.material = _material;
	}

	public get gameObject(): GameObject{
		return this._gameObject;
	}

	private create(): void {

		const cubeColors: Color[] = [];
		CubeData.faceColors.forEach(faceColor=>{
			for(let i=0; i<6; i++){
				//cubeColors.push(...faceColor);
				cubeColors.push(new Color(faceColor[0], faceColor[1], faceColor[2], faceColor[3]));
			}
		});

		const vertices: Vector[] = [];
		for(let v=0; v<CubeData.vertices.length; v+=3){
			vertices.push(new Vector(CubeData.vertices[v], CubeData.vertices[v+1], CubeData.vertices[v+2]));
		}

		const normals: Vector[] = [];
		for(let n=0; n<CubeData.normals.length; n+=3){
			normals.push(new Vector(CubeData.normals[n], CubeData.normals[n+1], CubeData.normals[n+2]));
		}

		const uvs: Vector[] = [];
		for(let tc=0; tc<CubeData.textureCoordinates.length; tc+=2){
			uvs.push(new Vector(CubeData.textureCoordinates[tc], CubeData.textureCoordinates[tc+1]));
		}

		this._gameObject.geometry.vertex = vertices;
		this._gameObject.geometry.vertexColor = cubeColors;
		this._gameObject.geometry.normal = normals;
		this._gameObject.geometry.uv = uvs;
	}
}

interface ICubeGeometryProps {
	vertices: number[];
	normals: number[];
	faceColors: number[][];
	textureCoordinates: number[];
};

export {
	Cube,
	ICubeGeometryProps
};

export const CubeData: ICubeGeometryProps = {
	vertices: [
		-0.5, -0.5, -0.5,
		0.5, -0.5, -0.5,
		0.5, 0.5, -0.5,
		0.5, 0.5, -0.5,
		-0.5, 0.5, -0.5,
		-0.5, -0.5, -0.5,
	
		-0.5, -0.5, 0.5,
		-0.5, 0.5, 0.5,
		0.5, 0.5, 0.5,
		0.5, 0.5, 0.5,
		0.5, -0.5, 0.5,
		-0.5, -0.5, 0.5,
	
		-0.5, 0.5, 0.5,
		-0.5, -0.5, 0.5,
		-0.5, -0.5, -0.5,
		-0.5, -0.5, -0.5,
		-0.5, 0.5, -0.5,
		-0.5, 0.5, 0.5,
		
		0.5, 0.5, 0.5,
		0.5, 0.5, -0.5,
		0.5, -0.5, -0.5,
		0.5, -0.5, -0.5,
		0.5, -0.5, 0.5,
		0.5, 0.5, 0.5,
	
		-0.5, -0.5, -0.5,
		-0.5, -0.5, 0.5,
		0.5, -0.5, 0.5,
		0.5, -0.5, 0.5,
		0.5, -0.5, -0.5,
		-0.5, -0.5, -0.5,

		-0.5, 0.5, -0.5,
		0.5, 0.5, -0.5,
		0.5, 0.5, 0.5,
		0.5, 0.5, 0.5,
		-0.5, 0.5, 0.5,
		-0.5, 0.5, -0.5
	],
	normals: [
		0.0,  0.0, -1.0,
		0.0,  0.0, -1.0,
		0.0,  0.0, -1.0,
		0.0,  0.0, -1.0,
		0.0,  0.0, -1.0,
		0.0,  0.0, -1.0,

		0.0,  0.0,  1.0,
		0.0,  0.0,  1.0,
		0.0,  0.0,  1.0,
		0.0,  0.0,  1.0,
		0.0,  0.0,  1.0,
		0.0,  0.0,  1.0,

		-1.0,  0.0,  0.0,
		-1.0,  0.0,  0.0,
		-1.0,  0.0,  0.0,
		-1.0,  0.0,  0.0,
		-1.0,  0.0,  0.0,
		-1.0,  0.0,  0.0,

		1.0,  0.0,  0.0,
		1.0,  0.0,  0.0,
		1.0,  0.0,  0.0,
		1.0,  0.0,  0.0,
		1.0,  0.0,  0.0,
		1.0,  0.0,  0.0,

		0.0, -1.0,  0.0,
		0.0, -1.0,  0.0,
		0.0, -1.0,  0.0,
		0.0, -1.0,  0.0,
		0.0, -1.0,  0.0,
		0.0, -1.0,  0.0,

		0.0,  1.0,  0.0,
		0.0,  1.0,  0.0,
		0.0,  1.0,  0.0,
		0.0,  1.0,  0.0,
		0.0,  1.0,  0.0,
		0.0,  1.0,  0.0
	],
	faceColors: [
		[1.0, 0.0, 0.0, 1.0],
		[0.0, 1.0, 0.0, 1.0],
		[0.0, 0.0, 1.0, 1.0],
		[1.0, 1.0, 0.0, 1.0],
		[1.0, 0.0, 1.0, 1.0],
		[0.0, 1.0, 1.0, 1.0]
	],
	textureCoordinates: [
		0.0, 0.0,
		1.0, 0.0,
		1.0, 1.0,
		1.0, 1.0,
		0.0, 1.0,
		0.0, 0.0,

		0.0, 0.0,
		1.0, 0.0,
		1.0, 1.0,
		1.0, 1.0,
		0.0, 1.0,
		0.0, 0.0,

		1.0, 0.0,
		1.0, 1.0,
		0.0, 1.0,
		0.0, 1.0,
		0.0, 0.0,
		1.0, 0.0,

		1.0, 0.0,
		1.0, 1.0,
		0.0, 1.0,
		0.0, 1.0,
		0.0, 0.0,
		1.0, 0.0,

		0.0, 1.0,
		1.0, 1.0,
		1.0, 0.0,
		1.0, 0.0,
		0.0, 0.0,
		0.0, 1.0,

		0.0, 1.0,
		1.0, 1.0,
		1.0, 0.0,
		1.0, 0.0,
		0.0, 0.0,
		0.0, 1.0
	]
};