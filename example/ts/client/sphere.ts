import { GameObject, Material, Vector, DrawModes, uuidv4, Color } from "../../../dist/ts/index.js";

interface ISphereProps{
	id?: string;
	segment?: number;
	stack?: number;
	radius?: number;
	drawMode?: DrawModes;
}

class Sphere {

	public id: string;

	private _gameObject: GameObject;

	private sphereProps: ISphereProps;

	private _segment: number = 32;
	private _stack: number = 16;
	private _radius: number = 1;

	constructor(sphereProps?: ISphereProps){
		this.setupDefaults(sphereProps);

		this._gameObject = new GameObject({
			drawMode: this.sphereProps.drawMode ?? DrawModes.TRIANGLES
		});

		this.create();
	}

	private setupDefaults(sphereProps?: ISphereProps): void{
		const newUUID = uuidv4();
		const _sphereProps = sphereProps ?? {id: newUUID};
		if(!_sphereProps.id) _sphereProps.id = newUUID;
		this.id = _sphereProps.id;

		if(!_sphereProps.segment) _sphereProps.segment = 32;
		if(!_sphereProps.stack) _sphereProps.stack = 16;
		if(!_sphereProps.radius) _sphereProps.radius = 1;

		if(!_sphereProps.drawMode) _sphereProps.drawMode = DrawModes.TRIANGLES;

		this.sphereProps = _sphereProps;
	}

	public get position(): Vector{
		return this._gameObject.position;
	}
	public set position(_position: Vector){
		this._gameObject.position = _position.clone();
	}

	public get rotation(): Vector{
		return this._gameObject.rotation;
	}
	public set rotation(_rotation: Vector){
		this._gameObject.rotation = _rotation.clone();
	}

	public get scale(): Vector{
		return this._gameObject.scale;
	}
	public set scale(_scale: Vector){
		this._gameObject.scale = _scale.clone();
	}

	public get material(): Material{
		return this._gameObject.material;
	}
	public set material(_material: Material){
		this._gameObject.material = _material;
	}

	public get gameObject(): GameObject{
		return this._gameObject;
	}

	private generateSphere(sectorCount: number, stackCount: number, radius: number): ISphereGeometryProps{

		const vertices: Vector[] = [];
		const vertexColors: Color[] = [];
		const normals: Vector[] = [];
		const textureCoordinates: Vector[] = [];
		const indices: number[] = [];
		const lineIndices: number[] = [];

		let x: number = 0, y: number = 0, z: number = 0, xz: number = 0;        // vertex position
		let nx: number = 0, ny: number = 0, nz: number = 0;
		const lengthInv: number = 1.0 / radius;                   				// vertex normal
		let s: number = 0, t: number = 0;                                       // vertex texCoord

		const sectorStep: number = 2 * Math.PI / sectorCount;
		const stackStep: number = Math.PI / stackCount;
		let sectorAngle: number, stackAngle: number;

		let colorCount = 1;
		let darkness = 1;

		for(let i = 0; i <= stackCount; i++){
			stackAngle = Math.PI / 2 - i * stackStep;        // starting from pi/2 to -pi/2
			xz = radius * Math.cos(stackAngle);             // r * cos(u)
			y = radius * Math.sin(stackAngle);              // r * sin(u)

			colorCount = 1;
			darkness = darkness===1 ? 0.5 : 1;

			// add (sectorCount+1) vertices per stack
			// the first and last vertices have same position and normal, but different tex coords
			for(let j = 0; j <= sectorCount; ++j){
				sectorAngle = j * sectorStep;           // starting from 0 to 2pi

				// vertex position (x, y, z)
				x = xz * Math.cos(sectorAngle);             // r * cos(u) * cos(v)
				z = xz * Math.sin(sectorAngle);             // r * cos(u) * sin(v)
				vertices.push(new Vector(x, y, z));

				let vertexColor = new Color(1 * darkness, 0, 0);
				if(colorCount % 3===0) vertexColor = new Color(0, 1 * darkness, 0);
				else if(colorCount % 2===0) vertexColor = new Color(0, 0, 1 * darkness);
				vertexColors.push(vertexColor);
				colorCount++;

				// normalized vertex normal (nx, ny, nz)
				nx = x * lengthInv;
				ny = y * lengthInv;
				nz = z * lengthInv;
				normals.push(new Vector(nx, ny, nz));

				// vertex tex coord (s, t) range between [0, 1]
				s = j / sectorCount;
				t = i / stackCount;
				textureCoordinates.push(new Vector(s, t));
			}
		}


		let k1: number, k2: number;
		for(let i = 0; i < stackCount; ++i){
			k1 = i * (sectorCount + 1);     // beginning of current stack
			k2 = k1 + sectorCount + 1;      // beginning of next stack

			for(let j = 0; j < sectorCount; ++j, ++k1, ++k2){
				// 2 triangles per sector excluding first and last stacks
				// k1 => k2 => k1+1
				if(i != 0){
					indices.push(k1, k2, k1 + 1);
				}

				// k1+1 => k2 => k2+1
				if(i != (stackCount-1)){
					indices.push(k1 + 1, k2, k2 + 1);
				}

				// store indices for lines
				// vertical lines for all stacks, k1 => k2
				lineIndices.push(k1, k2);
				if(i != 0){  // horizontal lines except 1st stack, k1 => k+1
					lineIndices.push(k1, k1 + 1);
				}
			}
		}

		return {
			vertices,
			vertexColors,
			normals,
			textureCoordinates,
			indices,
			lineIndices
		};

	}

	private create(): void {

		const {
			vertices,
			vertexColors,
			normals,
			textureCoordinates,
			indices,
			lineIndices
		} = this.generateSphere(this.sphereProps.segment ?? 32, this.sphereProps.stack ?? 16, this.sphereProps.radius ?? 1);

		this._gameObject.geometry.vertex = vertices;
		this._gameObject.geometry.vertexColor = vertexColors;
		this._gameObject.geometry.normal = normals;
		this._gameObject.geometry.uv = textureCoordinates;
		this._gameObject.geometry.index = indices;
	}
}

interface ISphereGeometryProps {
	vertices: Vector[];
	vertexColors: Color[];
	normals: Vector[];
	textureCoordinates: Vector[];
	indices: number[];
	lineIndices: number[];
};

export {
	Sphere,
	ISphereGeometryProps
};