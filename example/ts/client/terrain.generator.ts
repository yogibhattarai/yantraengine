/* eslint-disable @typescript-eslint/ban-ts-comment */
// @ts-ignore
import { noise } from "../../esm/lib/perlin";

import { generateSeed, inverseLerp, randomInt, Vector, Color } from "../../../dist/ts/index.js";

interface ITerrainHeightAndColor{
	height: number;
	color: Color;
}

interface ITerrainDataProps{
	[key: string]: ITerrainHeightAndColor;
}

interface ITerrainGeneratorProps{
	seed?: string;
	width?: number;
	height?: number;
	octaves?: number;
	offset?: Vector;
	noiseScale?: number;
	persistence?: number;
	lacunarity?: number;
}

interface ICoordinates{
	x: number;
	y: number;
}

interface INoiseMap{
	heights: number[][];
	colors: Color[][];
}

class TerrainGenerator{

	private terrainGeneratorProps: ITerrainGeneratorProps;
	private terrainDataProps: ITerrainDataProps;

	constructor(tgProps?: ITerrainGeneratorProps){
		//console.log(noise);

		this.setupDefaultProps(tgProps);

		this.setupDefaultTerrainProps();
	}

	private setupDefaultProps(tgProps?: ITerrainGeneratorProps): void{
		const defaultTGProps = (this.terrainGeneratorProps ? {...this.terrainGeneratorProps, ...tgProps} : tgProps) ?? {};
		if(!defaultTGProps?.seed) defaultTGProps.seed = `${generateSeed()}`;
		if(!defaultTGProps?.width || defaultTGProps?.width < 0) defaultTGProps.width = 10;
		if(!defaultTGProps?.height || defaultTGProps?.height < 0) defaultTGProps.height = 10;
		if(!defaultTGProps?.octaves || defaultTGProps?.octaves < 0) defaultTGProps.octaves = 4;
		if(!defaultTGProps?.offset) defaultTGProps.offset = new Vector();
		if(!defaultTGProps?.noiseScale || defaultTGProps?.noiseScale < 0) defaultTGProps.noiseScale = 150;
		if(!defaultTGProps?.persistence || defaultTGProps?.persistence < 0) defaultTGProps.persistence = 0.5;
		if(!defaultTGProps?.lacunarity || defaultTGProps?.lacunarity < 0) defaultTGProps.lacunarity = 2;

		this.terrainGeneratorProps = defaultTGProps;
		
		// @ts-ignore
		// eslint-disable-next-line @typescript-eslint/no-unsafe-call, @typescript-eslint/no-unsafe-member-access
		noise.seed(this.terrainGeneratorProps.seed);
	}
	public updateProps(tgProps?: ITerrainGeneratorProps): void{
		this.setupDefaultProps(tgProps);
	}

	private setupDefaultTerrainProps(): void{
		this.terrainDataProps = {
			WaterDeep: {
				height: 0.3,
				color: new Color(13, 41, 98)
			},
			WaterShallow: {
				height: 0.4,
				color: new Color(52, 101, 192)
			},
			Sand: {
				height: 0.5,
				color: new Color(215, 204, 132)
			},
			Grass: {
				height: 0.6,
				color: new Color(58, 195, 30)
			},
			Thicket: {
				height: 0.7,
				color: new Color(27, 140, 10)
			},
			LooseRock: {
				height: 0.8,
				color: new Color(73, 47, 33)
			},
			HardRock: {
				height: 0.9,
				color: new Color(53, 47, 44)
			},
			Snow: {
				height: 1.0,
				color: new Color(255, 255, 255)
			},
		};
	}

	public generate(): INoiseMap{
		const mapwidth: number = this.terrainGeneratorProps.width ?? 10;
		const mapheight: number = this.terrainGeneratorProps.height ?? 10;

		const octaveoffsets: ICoordinates[] = [];
		for(let octloop=0; octloop<(this.terrainGeneratorProps?.octaves ?? 4); octloop++){
			const offsetX = randomInt(-100000, 100000, this.terrainGeneratorProps.seed) + (this.terrainGeneratorProps.offset?.x ?? 0);
			const offsetY = randomInt(-100000, 100000, this.terrainGeneratorProps.seed) + (this.terrainGeneratorProps.offset?.y ?? 0);
			octaveoffsets.push({x: offsetX, y: offsetY});
		}

		let maxnoiseheight = -Infinity;
		let minnoiseheight = Infinity;

		const halfwidth = mapwidth/2;
		const halfheight = mapheight/2;

		const noisemap: number[][] = [];
		const colormap: Color[][] = [];

		const terrainDataPropsObjectArray = Object.values(this.terrainDataProps);
		const terrainDataMappedHeights = terrainDataPropsObjectArray.map(props=>props.height);

		for(let x=0; x < mapwidth; x++){
			
			noisemap[x] = [];
			colormap[x] = [];

			for(let y=0; y < mapheight; y++){

				let amplitude = 1;
				let frequency = 1;
				let noiseheight = 0;

				for(let oct=0; oct<(this.terrainGeneratorProps?.octaves ?? 4); oct++){

					const sampleX = (x-halfwidth)/(this.terrainGeneratorProps.noiseScale ?? 150) * frequency + octaveoffsets[oct].x;
					const sampleY = (y-halfheight)/(this.terrainGeneratorProps.noiseScale ?? 150) * frequency + octaveoffsets[oct].y;

					// @ts-ignore
					// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment, @typescript-eslint/no-unsafe-call, @typescript-eslint/no-unsafe-member-access
					const perlinvalue: number = noise.perlin2(sampleX, sampleY);//noise.simplex2(sampleX, sampleY);

					noiseheight += perlinvalue * amplitude;

					amplitude *= this.terrainGeneratorProps.persistence ?? 0.5;
					frequency *= this.terrainGeneratorProps.lacunarity ?? 2;

					if(noiseheight > maxnoiseheight) maxnoiseheight = noiseheight;
					else if(noiseheight < minnoiseheight) minnoiseheight = noiseheight;

					noisemap[x][y] = noiseheight;
				}

				const normalizednoiseheight = inverseLerp(minnoiseheight, maxnoiseheight, noisemap[x][y]);

				const closestHeight = terrainDataMappedHeights.reduce((prevValue, currValue)=>{
					const closestHeightObject = (Math.abs(currValue - Math.abs(normalizednoiseheight)) < Math.abs(prevValue - Math.abs(normalizednoiseheight)) ? currValue : prevValue);
					return closestHeightObject;
				});
				const foundTerrainDataProps = terrainDataPropsObjectArray.find(props=>props.height===closestHeight);

				colormap[x][y] = (foundTerrainDataProps?.color as Color).clone();

			}
		}

		return {
			heights: noisemap,
			colors: colormap
		};

	}
}

export {
	TerrainGenerator,
	ITerrainGeneratorProps,
	INoiseMap
};