// eslint-disable-next-line @typescript-eslint/no-var-requires
const path = require("path");
// eslint-disable-next-line @typescript-eslint/no-var-requires
const webpack = require("webpack");

const webpackConfig = {
    target: "node",
    node: {
        global: true
    },
    mode: "development",
    optimization: {
        minimize: false
    },
    devtool: "inline-source-map",
    context: __dirname,
    entry: {
        app: [
            "@babel/polyfill",
            path.resolve(__dirname, "client", "index.ts")
        ]
    },
    module: {
        rules: [
            { test: /\.(js|jsx)$/, use:[{loader: "babel-loader"}], exclude: /node_modules/ },
            { test: /\.(ts|tsx)$/, use:[{ loader: "ts-loader", options: { configFile: path.resolve(__dirname, "tsconfig.example.json") } }], exclude: /node_modules/ },
            { test: /\.css$/, use:[{loader: "style-loader"}, {loader: "css-loader"}] },
            { test: /\.scss$/, use:[{loader: "style-loader"}, {loader: "css-loader"}, {loader: "sass-loader"}] },
            { test: /\.(png|jpg|gif|svg)$/, use:[{loader: "file-loader"}] },
            { test: /\.(ttf|woff|woff2|eot)$/, use:[{loader: "url-loader"}] }
        ]
    },
    resolve: {
        alias: {
            "@env": path.resolve(__dirname, "server", "env.ts")
        },
        extensions: [".js", ".jsx", ".ts", ".tsx"],
    },
    output: {
        filename: "[name].[contenthash].js",
        path: path.resolve(__dirname, "dist")
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin()
    ]
};

module.exports = webpackConfig;