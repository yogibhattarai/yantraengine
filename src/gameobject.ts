import { Geometry } from "./geometry";
import { Vector, uuidv4 } from "./utilities";
import { mat4, vec3 } from "gl-matrix";
import { Shader } from "./shader";
import type { IShaderObject, IAttribute, IUniform } from "./shader";
import type {  ICameraMatrix } from "./camera";
import { Material } from "./material";
import type { ILightProps } from "./light";

interface IGameObjectProps{
	id?: string;
	faceDirection?: EFaceDirection;
	drawMode?: EDrawModes;
}

class GameObject {
	public static gl: WebGL2RenderingContext;

	public id: string;
	private gameObjectProps: IGameObjectProps;

	public geometry: Geometry;
	private shader: Shader;
	private _material: Material;

	private _position: Vector = new Vector(0, 0, 0);
	private _rotation: Vector = new Vector(0, 0, 0);
	private _scale: Vector = new Vector(1, 1, 1);

	public modelMatrix: mat4;
	public normalMatrix: mat4;

	constructor(gameObjectProps?: IGameObjectProps){
		this.setupDefaults(gameObjectProps);

		this.geometry = new Geometry();
		this.shader = new Shader();
		this.modelMatrix = mat4.create();
		this.normalMatrix = mat4.create();
		mat4.invert(this.normalMatrix, this.modelMatrix);
		mat4.transpose(this.normalMatrix, this.normalMatrix);
	}

	private setupDefaults(gameObjectProps?: IGameObjectProps): void{
		const newUUID = uuidv4();
		const defaultGameObjectProps = gameObjectProps ?? {id: newUUID};
		if(!defaultGameObjectProps.id) defaultGameObjectProps.id = newUUID;
		this.id = defaultGameObjectProps.id;

		if(!defaultGameObjectProps.faceDirection) defaultGameObjectProps.faceDirection = EFaceDirection.CW;
		if(!defaultGameObjectProps.drawMode) defaultGameObjectProps.drawMode = EDrawModes.TRIANGLES;

		this.gameObjectProps = defaultGameObjectProps;
	}

	public get position(): Vector{
		return this._position;
	}
	public set position(_position: Vector){
		this._position = _position.clone();
		this.updateModelMatrix();
	}

	public get rotation(): Vector{
		return this._rotation;
	}
	public set rotation(_rotation: Vector){
		this._rotation = _rotation.clone();
		this.updateModelMatrix();
	}

	public get scale(): Vector{
		return this._scale;
	}
	public set scale(_scale: Vector){
		this._scale = _scale.clone();
		this.updateModelMatrix();
	}

	public get material(): Material{
		return this._material;
	}
	public set material(_material: Material){
		this._material = _material;
		// recompile shaders here
	}

	public get drawMode(): EDrawModes{
		return this.gameObjectProps.drawMode ?? EDrawModes.TRIANGLES;
	}
	public set drawMode(newDrawMode: EDrawModes){
		this.gameObjectProps.drawMode = newDrawMode;
	}

	public async create(cameraMatrix: ICameraMatrix, lightProps: ILightProps[]): Promise<boolean>{
		if(!this._material) this._material = new Material();
		
		try{
			await this._material.loadAssets();

			this.updateModelMatrix();

			const geometryAttributes: IAttribute[] = this.geometry.generateAttributes();

			const cameraUniforms: IUniform[] = [
				{
					name: "u_projectionMatrix",
					data: cameraMatrix.projectionMatrix
				},
				{
					name: "u_viewMatrix",
					data: cameraMatrix.viewMatrix
				}
			];

			const modelUniforms: IUniform[] = [
				{
					name: "u_modelMatrix",
					data: this.modelMatrix
				},
				{
					name: "u_normalMatrix",
					data: this.normalMatrix
				}
			];

			const materialUniforms: IUniform[] = this._material.generateUniforms();

			const shaderObject: IShaderObject = {
				attributes: geometryAttributes,
				uniforms: [...cameraUniforms, ...modelUniforms, ...materialUniforms]
			};

			const shaderTexts = this.material.generateShaderTexts(lightProps);
			this.shader.create(shaderObject, shaderTexts);

			return Promise.resolve(true);
		}
		catch(err){
			console.error(err);
		}

		return false;
	}

	public draw(): void{
		GameObject.gl.frontFace(GameObject.gl[this.gameObjectProps.faceDirection ?? "CW"]);

		this.shader.useVAO();
		
		if(this.geometry && this.geometry.vertex?.length){
			if(this.geometry.index?.length) GameObject.gl.drawElements(GameObject.gl[this.gameObjectProps.drawMode ?? "TRIANGLES"], this.geometry.index.length, GameObject.gl.UNSIGNED_SHORT, 0);
			else GameObject.gl.drawArrays(GameObject.gl[this.gameObjectProps.drawMode ?? "TRIANGLES"], 0, this.geometry.vertex.length);
		}
	}
	
	private updateModelMatrix(): void{
		mat4.identity(this.modelMatrix);

		// Translation must be done after global rotation, before local rotation
		mat4.translate(this.modelMatrix, this.modelMatrix, vec3.fromValues(this._position.x, this._position.y, -1 * this._position.z));

		// This is local rotation
		mat4.rotateX(this.modelMatrix, this.modelMatrix, this._rotation.x);
		mat4.rotateY(this.modelMatrix, this.modelMatrix, this._rotation.y);
		mat4.rotateZ(this.modelMatrix, this.modelMatrix, this._rotation.z);

		// Scale doesn't need to be last, but it certainly needs to be after translation
		mat4.scale(this.modelMatrix, this.modelMatrix, vec3.fromValues(this._scale.x, this._scale.y, this._scale.z));

		mat4.identity(this.normalMatrix);
		mat4.invert(this.normalMatrix, this.modelMatrix);
		mat4.transpose(this.normalMatrix, this.normalMatrix);

		this.updateShaderProps(
			{
				attributes: [],
				uniforms: [
					{
						name: "u_modelMatrix",
						data: this.modelMatrix
					},
					{
						name: "u_normalMatrix",
						data: this.normalMatrix
					}
				]
			}
		);
	}

	public updateCameraMatrix(cameraMatrix: ICameraMatrix): void{
		this.updateShaderProps(
			{
				attributes: [],
				uniforms: [
					{
						name: "u_projectionMatrix",
						data: cameraMatrix.projectionMatrix
					},
					{
						name: "u_viewMatrix",
						data: cameraMatrix.viewMatrix
					}
				]
			}
		);
	}

	public updateLightProps(lightProps: ILightProps[]): void{
		const shaderTexts = this.material.generateShaderTexts(lightProps);
		this.shader.recreate(shaderTexts);
	}

	private updateShaderProps(shaderProps: IShaderObject): void{
		if(this.shader.compiled) this.shader.updateShaderProps(shaderProps);
	}

	public updateShaderGeometryAttributes(): void{
		const updatedGeometryAttributes: IAttribute[] = this.geometry.generateAttributes();
		if(this.shader.compiled) this.shader.updateShaderProps({
			attributes: updatedGeometryAttributes,
			uniforms: []
		});
	}
}

enum EDrawModes {
	TRIANGLES = "TRIANGLES",
	LINES = "LINES",
	POINTS = "POINTS",
	TRIANGLE_STRIP = "TRIANGLE_STRIP",
	TRIANGLE_FAN = "TRIANGLE_FAN",
	LINE_STRIP = "LINE_STRIP",
	LINE_LOOP = "LINE_LOOP",
}

enum EFaceDirection {
	CW = "CW",
	CCW = "CCW"
}

export {
	GameObject,
	EDrawModes,
	EFaceDirection
};