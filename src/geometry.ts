import { Vector, Color, uuidv4 } from "./utilities";
import type { IAttribute } from "./shader";

interface IGeometryProps {
	id?: string;
	instanced?: boolean;
	numOfInstance?: number;
}

class Geometry{
	public static gl: WebGL2RenderingContext;

	public id: string;
	private geometryProps: IGeometryProps;

	// Change this to protected
	public vertexF32: Float32Array;
	public vertexColorF32: Float32Array;
	public uvF32: Float32Array;
	public normalF32: Float32Array;
	public indexU16: Uint16Array;

	private _vertex: Vector[] = [];
	private _vertexColor: Color[] = [];
	private _uv: Vector[] = [];
	private _normal: Vector[] = [];
	private _index: number[] = [];

	constructor(geomProps?: IGeometryProps){
		this.setupDefaults(geomProps);
	}

	private setupDefaults(geomProps?: IGeometryProps): void{
		const newUUID = uuidv4();
		const defaultGeomProps = geomProps ?? {id: newUUID};
		if(!defaultGeomProps.id) defaultGeomProps.id = newUUID;
		this.id = defaultGeomProps.id;

		if(!defaultGeomProps?.instanced) defaultGeomProps.instanced = false;
		if(!defaultGeomProps?.numOfInstance) defaultGeomProps.numOfInstance = 1;
		
		this.geometryProps = defaultGeomProps;
	}

	public get vertex(): Vector[]{
		return this._vertex;
	}
	public set vertex(_vertex: Vector[]){
		if(_vertex?.length){
			const vertices: number[] = [];
			for(let v=0; v<_vertex.length; v++){
				const _vertex_ = _vertex[v] ?? new Vector();
				vertices.push(_vertex_.x);
				vertices.push(_vertex_.y);
				vertices.push(_vertex_.z);
			}
			this.vertexF32 = new Float32Array(vertices);
		}
		this._vertex = _vertex;
	}

	public get vertexColor(): Color[]{
		return this._vertexColor;
	}
	public set vertexColor(_vertexColor: Color[]){
		if(_vertexColor?.length){
			const vertexColors: number[] = [];
			for(let vc=0; vc<_vertexColor.length; vc++){
				const _vertexColor_ = _vertexColor[vc] ?? new Color();
				vertexColors.push(_vertexColor_.r);
				vertexColors.push(_vertexColor_.g);
				vertexColors.push(_vertexColor_.b);
				vertexColors.push(_vertexColor_.a);
			}
			this.vertexColorF32 = new Float32Array(vertexColors);
		}
		this._vertexColor = _vertexColor;
	}

	public get uv(): Vector[]{
		return this._uv;
	}
	public set uv(_uv: Vector[]){
		if(_uv?.length){
			const uvs: number[] = [];
			for(let uvc=0; uvc<_uv.length; uvc++){
				const _uv_ = _uv[uvc] ?? new Vector();
				uvs.push(_uv_.x);
				uvs.push(_uv_.y);
			}
			this.uvF32 = new Float32Array(uvs);
		}
		this._uv = _uv;
	}

	public get normal(): Vector[]{
		return this._normal;
	}
	public set normal(_normal: Vector[]){
		if(_normal?.length){
			const normals: number[] = [];
			for(let nc=0; nc<_normal.length; nc++){
				const _normal_ = _normal[nc] ?? new Vector();
				normals.push(_normal_.x);
				normals.push(_normal_.y);
				normals.push(_normal_.z);
			}
			this.normalF32 = new Float32Array(normals);
		}
		this._normal = _normal;
	}

	public get index(): number[]{
		return this._index;
	}
	public set index(_index: number[]){
		if(_index?.length){
			this._index = _index;
			this.indexU16 = new Uint16Array(this._index);
		}
	}

	public generateAttributes(): IAttribute[]{
		const attributes: IAttribute[] = [];

		if(this.vertexF32?.length){
			attributes.push({
				name: "a_position",
				dataType: Geometry.gl.FLOAT,
				data: this.vertexF32,
				dimension: 3
			});
		}
		if(this.vertexColorF32?.length){
			attributes.push({
				name: "a_color",
				dataType: Geometry.gl.FLOAT,
				data: this.vertexColorF32,
				dimension: 4
			});
		}
		if(this.uvF32?.length){
			attributes.push({
				name: "a_uv",
				dataType: Geometry.gl.FLOAT,
				data: this.uvF32,
				dimension: 2
			});
		}
		if(this.normalF32?.length){
			attributes.push({
				name: "a_normal",
				dataType: Geometry.gl.FLOAT,
				data: this.normalF32,
				dimension: 3
			});
		}
		if(this.indexU16?.length){
			attributes.push({
				name: "a_index",
				dataType: "Index",
				data: this.indexU16,
			});
		}

		return attributes;
	}
}

export {
	Geometry,
	IGeometryProps
};