import { mat4, vec3 } from "gl-matrix";
import type { Scene } from "./scene";
import { toRadian, Vector, uuidv4 } from "./utilities";

interface ICameraProps{
	id?: string;
}

abstract class Camera {
	public static gl: WebGL2RenderingContext;
	
	public id: string;
	private cameraProps: ICameraProps;

	protected parentScene: Scene;

	public projectionMatrix: mat4;
	public viewMatrix: mat4;
	public transformMatrix: mat4;
	public projectionViewMatrix: mat4;

	public _position: Vector = new Vector(0.0, 0.0, -2.0);
	public _lookAt: Vector = new Vector(0.0, 0.0, 0.0);
	public _up: Vector = new Vector(0.0, 1.0, 0.0);

	constructor(cameraProps?: ICameraProps){
		this.setupDefaults(cameraProps);

		this.projectionMatrix = mat4.create();
		this.viewMatrix = mat4.create();
		this.transformMatrix = mat4.create();
		this.projectionViewMatrix = mat4.create();
	}

	private setupDefaults(cameraProps?: ICameraProps): void{
		const newUUID = uuidv4();
		const defaultCameraProps = cameraProps ?? {id: newUUID};
		if(!defaultCameraProps.id) defaultCameraProps.id = newUUID;
		this.id = defaultCameraProps.id;

		this.cameraProps = defaultCameraProps;
	}

	public get position(): Vector{
		return this._position;
	}
	public set position(_position: Vector){
		this._position = _position.clone();
		this.applyTransforms();
		if(this.parentScene) this.parentScene.updateCameraMatrix();
	}

	public get lookAt(): Vector{
		return this._lookAt;
	}
	public set lookAt(_lookAt: Vector){
		this._lookAt = _lookAt.clone();
		this.applyTransforms();
		if(this.parentScene) this.parentScene.updateCameraMatrix();
	}

	public get up(): Vector{
		return this._up;
	}
	public set up(_up: Vector){
		this._up = _up.clone();
		this.applyTransforms();
		if(this.parentScene) this.parentScene.updateCameraMatrix();
	}
	
	public abstract activate(): void;

	public applyTransforms(): void{

		mat4.identity(this.transformMatrix);
		mat4.identity(this.viewMatrix);

		// I don't think this works at all
		mat4.translate(this.transformMatrix, this.transformMatrix, vec3.fromValues(this._position.x, this._position.y, this._position?.z ?? 0.0));

		// Why am I even doing this? Doesn't this just move the world instead of camera?
		mat4.invert(this.viewMatrix, this.transformMatrix);

		mat4.lookAt(
			this.viewMatrix,
			vec3.fromValues(this._position.x, this._position.y, this._position?.z ?? -2.0),
			vec3.fromValues(this._lookAt.x, this._lookAt.y, this._lookAt?.z ?? 0.0),
			vec3.fromValues(this._up.x, this._up.y, this._up?.z ?? 0.0)
		);

		mat4.multiply(this.projectionViewMatrix, this.projectionMatrix, this.viewMatrix);
	}

	public setParentScene(scene: Scene): void{
		this.parentScene = scene;
	}
}

class PerspectiveCamera extends Camera{

	private fovInDegrees: number = 75;
	private _aspectRatio: number = 0;
	private nearPlane: number = 0.1;
	private farPlane: number = 1000;

	constructor(props?: IPerspectiveCameraProps){
		super();

		if(props?.fov) this.fovInDegrees = props.fov;
		if(props?.aspect) this._aspectRatio = props.aspect;
		if(props?.near) this.nearPlane = props.near;
		if(props?.far) this.farPlane = props.far;
	}

	public get aspectRatio(): number{
		return this._aspectRatio;
	}
	public set aspectRatio(_aspectRatio: number){
		if(_aspectRatio>0){
			this._aspectRatio = _aspectRatio;
			this.activate();
		}
	}

	public override activate(): void {
		if(!this._aspectRatio) this._aspectRatio = Camera.gl.canvas.width/Camera.gl.canvas.height;

		mat4.identity(this.projectionMatrix);
		mat4.perspective(this.projectionMatrix, toRadian(this.fovInDegrees), this._aspectRatio, this.nearPlane, this.farPlane);

		this.applyTransforms();
	}
}

class OrthographicCamera extends Camera{

	private left: number = -1;
	private right: number = 1;
	private bottom: number = -1;
	private top: number = 1;
	private nearPlane: number = 0.1;
	private farPlane: number = 1000;

	constructor(props?: IOrthographicCameraProps){
		super();

		if(props?.left) this.left = props.left;
		if(props?.right) this.right = props.right;
		if(props?.bottom) this.bottom = props.bottom;
		if(props?.top) this.top = props.top;
		if(props?.near) this.nearPlane = props.near;
		if(props?.far) this.farPlane = props.far;
	}

	public override activate(): void {
		mat4.identity(this.projectionMatrix);
		mat4.ortho(this.projectionMatrix, this.left, this.right, this.bottom, this.top, this.nearPlane, this.farPlane);

		this.applyTransforms();
	}
}

interface IPerspectiveCameraProps {
	fov?: number;
	aspect?: number;
	near?: number;
	far?: number;
}

interface IOrthographicCameraProps {
	left?: number;
	right?: number;
	bottom?: number;
	top?: number;
	near?: number;
	far?: number;
}

interface ICameraMatrix{
	projectionMatrix: mat4;
	viewMatrix: mat4;
	projectionViewMatrix?: mat4;
	transformMatrix?: mat4;
}

export {
	Camera,
	PerspectiveCamera,
	OrthographicCamera,

	ICameraMatrix
};