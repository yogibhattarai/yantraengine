import { Scene } from "./scene";
import { Camera } from "./camera";
import { GameObject } from "./gameobject";
import { Geometry } from "./geometry";
import { Shader } from "./shader";

class Engine {

	private props: IEngineProps;
	public canvas: HTMLCanvasElement;
	public glContext: WebGL2RenderingContext;

	/*********************************************/
	// Looping related attribs and methods
	// This will probably move to a separate class. For now it's here
	private fps: number = 0;
	private fpsinterval: number = 0;
	private lockframe: boolean;
	private then: number = 0;
	private delta: number = 0;
	private loopId: number = 0;
	/*********************************************/

	private scenes: Scene[];
	private activeScene: Scene | undefined;

	private preRenderCallback: ()=>void;
	private postRenderCallback: ()=>void;

	constructor(props?: IEngineProps){
		this.props = this.setDefaultProps(props);

		/********* See line number 12 *********/
		this.fps = this.props.fps ?? 0;
		this.lockframe = this.fps ? true : false;
		this.fpsinterval = this.fps ? (1000 / this.fps) - 0.1 : 0;
		this.then = performance.now();
		/**************************************/

		this.canvas = this.props.canvas as HTMLCanvasElement;

		const glContextOptions: WebGLContextAttributes = {
			alpha: false,
			depth: true,
			preserveDrawingBuffer: false
		};
		this.glContext = this.canvas.getContext("webgl2", glContextOptions) as WebGL2RenderingContext;
		if(!this.glContext) throw Error("WebGL2 context unavailable in this browser.");

		this.glContext.enable(this.glContext.CULL_FACE);
		this.glContext.cullFace(this.glContext.BACK);

		this.glContext.enable(this.glContext.DEPTH_TEST);

		this.glContext.enable(this.glContext.BLEND);
		this.glContext.blendFunc(this.glContext.SRC_ALPHA, this.glContext.ONE_MINUS_SRC_ALPHA);

		this.glContext.viewport(0, 0, this.glContext.canvas.width, this.glContext.canvas.height);

		this.setGLContexts();
	}

	private setGLContexts(): void{
		Scene.gl = this.glContext;
		Camera.gl = this.glContext;
		GameObject.gl = this.glContext;
		Geometry.gl = this.glContext;
		Shader.gl = this.glContext;
	}

	private setDefaultProps(props?: IEngineProps): IEngineProps{
		if(!props) props = {};
		if(!props.canvas){
			props.canvas = document.createElement("canvas");
			props.canvas.id = "rendercanvas";
			props.canvas.width = 854;
			props.canvas.height = 480;

			document.body.appendChild(props.canvas);
		}
		else if(typeof props.canvas==="string"){
			const canvasId = `#${props.canvas}`;
			props.canvas = document.querySelector(canvasId) as HTMLCanvasElement;
		}
		if(props.fullScreen){
			props.canvas.width = window.innerWidth;
			props.canvas.height = window.innerHeight;
		}

		if(isNaN(Number(props?.fps)) || !props?.fps){
			props.fps = 0;
		}

		return props;
	}

	private loop(now: number): void{
		if(this.preRenderCallback) this.preRenderCallback();

		this.loopId = requestAnimationFrame(this.loop.bind(this));

		const delta = now - this.then;

		/* This locks framerate */
		if(delta >= this.fpsinterval){
			this.then = now - (delta % this.fpsinterval);
			if(this.lockframe) this.render();
		}
		/* **************** */

		/* This doesn't */
		if(!this.lockframe){
			this.render();
		}
		/* ************ */

		if(this.postRenderCallback) this.postRenderCallback();
	}

	public async compileScene(scene: Scene): Promise<void>{
		try{
			await scene.compile();
		}
		catch(err){
			console.error("Could not compile scene!!!", err);
		}
	}

	public async addScene(scene: Scene): Promise<void>{
		if(!this.scenes?.length) this.scenes = [scene];
		else this.scenes.push(scene);

		try{
			await this.loadScene(this.scenes[this.scenes.length - 1]);
		}
		catch(err){
			console.error("Could not load scene!!!", err);
		}
	}

	public async loadScene(scene: Scene, callback?: ()=>void): Promise<void>{
		// Compile everything related to the scene if necessary
		this.activeScene = scene;
		await this.activeScene.compile();

		if(callback) callback();
	}

	public disposeScene(scene: Scene, callback?: ()=>void): void{
		// Compile everything related to the scene if necessary
		if(this.activeScene?.id === scene.id){
			this.activeScene = undefined;
		}

		if(callback) callback();
	}

	public render(scene?: Scene): void{
		if(scene) scene.render();
		else if(this.activeScene) this.activeScene.render();
		else console.error("No scene passed to be rendered and no scene activated yet!!!");

		if(this.glContext.getError()) console.log("GL Error: ", this.glContext.getError(), ", [0] == No Error");
	}

	public run(...args: (()=>void)[]): void{
		if(args?.length>2) throw new Error("Run method can only have 2 parameters. Both must be functions. One is for pre render and the other one is for post render callbacks!!!");
		if(args[0]) this.preRenderCallback = args[0];
		if(args[1]) this.postRenderCallback = args[1];

		this.loop(performance.now());
	}
}

interface IEngineProps {
	canvas?: HTMLCanvasElement | string;			// Generally we want typescript config to not allow multiple types, but for Engine class we need it.
	fullScreen?: boolean;
	fps?: number;
};

interface IEngineRunProps {
	beforeRender?: ()=>void;
	afterRender?: ()=>void;
}

export {
	IEngineProps,
	IEngineRunProps
};

export default Engine;
export type { Engine };