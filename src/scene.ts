import { Camera, PerspectiveCamera } from "./camera";
import { Light, ELightType, AmbientLight } from "./light";
import type { ILightProps } from "./light";
import { GameObject } from "./gameobject";
import { Color, uuidv4 } from "./utilities";

interface ISceneProps{
	id?: string;
}

class Scene {
	public static gl: WebGL2RenderingContext;

	public id: string;
	private sceneProps: ISceneProps;

	private gameObjects: GameObject[];
	private cameras: Camera[];
	private lights: Light[];

	public activeCamera: Camera;

	private renderReady: boolean = false;

	constructor(sceneProps?: ISceneProps){
		this.setupDefaults(sceneProps);

		this.gameObjects = new Array<GameObject>();
		this.cameras = new Array<Camera>();
		this.lights = new Array<Light>();
	}

	private setupDefaults(sceneProps?: ISceneProps): void{
		const newUUID = uuidv4();
		const defaultSceneProps = sceneProps ?? {id: newUUID};
		if(!defaultSceneProps.id) defaultSceneProps.id = newUUID;
		this.id = defaultSceneProps.id;

		this.sceneProps = defaultSceneProps;
	}

	public add(objectArg: (Camera | Light | GameObject)): void{
		if(objectArg instanceof Camera && !this.cameras.find(camera=>camera.id===objectArg.id)){
			objectArg.setParentScene(this);
			this.cameras.push(objectArg);
		}
		else if(objectArg instanceof Light && !this.lights.find(light=>light.id===objectArg.id)){
			const directionalLightExists = this.lights.find(light=>light.props.type===ELightType.Directional && objectArg.props.type===ELightType.Directional);
			const ambientLightExists = this.lights.find(light=>light.props.type===ELightType.Ambient && objectArg.props.type===ELightType.Ambient);
			if(directionalLightExists || ambientLightExists) throw new Error("You can only have one directional and one ambient light per scene!!!");
			else{
				objectArg.setParentScene(this);
				this.lights.push(objectArg);
			}
		}
		else if(objectArg instanceof GameObject && !this.gameObjects.find(gameObject=>gameObject.id===objectArg.id)){
			this.gameObjects.push(objectArg);
		}
	}

	public async remove(objectArg: (Camera | Light | GameObject)): Promise<void>{
		this.renderReady = false;

		if(objectArg instanceof Camera && this.cameras.find(camera=>camera.id===objectArg.id)){
			this.cameras = this.cameras.filter(camera=>camera.id!==objectArg.id);
		}
		else if(objectArg instanceof Light && this.lights.find(light=>light.id===objectArg.id)){
			this.lights = this.lights.filter(light=>light.id!==objectArg.id);
		}
		else if(objectArg instanceof GameObject && this.gameObjects.find(gameObject=>gameObject.id===objectArg.id)){
			this.gameObjects = this.gameObjects.filter(gameObject=>gameObject.id!==objectArg.id);
		}

		await this.compile();
	}

	private generateLightProps(): ILightProps[]{
		const lightProps: ILightProps[] = [];

		for(let lc=0; lc<this.lights.length; lc++){
			lightProps.push(this.lights[lc].props);
		}

		return lightProps;
	}

	public async compile(): Promise<boolean>{

		if(!this.cameras?.length) this.cameras.push(new PerspectiveCamera());
		if(!this.activeCamera) this.activeCamera = this.cameras[0];

		this.activeCamera?.activate();

		const ambientLight: Light = this.lights.find(light=>light.props.type===ELightType.Ambient) as Light;
		if(!ambientLight) this.add(new AmbientLight({
			color: new Color(0.1, 0.1, 0.1)
		}));

		const lightProps: ILightProps[] = this.generateLightProps();

		const gameObjectCreationPromises: Promise<boolean>[] = [];
		for(let go=0; go<this.gameObjects.length; go++){
			gameObjectCreationPromises.push(
				this.gameObjects[go]?.create(
					{
						projectionMatrix: this.activeCamera?.projectionMatrix,
						viewMatrix: this.activeCamera?.viewMatrix,
						projectionViewMatrix: this.activeCamera?.projectionViewMatrix,
						transformMatrix: this.activeCamera?.transformMatrix
					},
					lightProps
				)
			);
		}

		try{
			await Promise.allSettled(gameObjectCreationPromises);
			this.renderReady = true;
			return this.renderReady;
		}
		catch(err){
			console.error(err);
			return this.renderReady;
		}
	}

	public render(): void{
		if(this.renderReady){
			Scene.gl.clearColor(0.18, 0.24, 0.31, 1.0);
			Scene.gl.clear(Scene.gl.COLOR_BUFFER_BIT | Scene.gl.DEPTH_BUFFER_BIT);

			for(let go=0; go<this.gameObjects.length; go++){
				this.gameObjects[go]?.draw();
			}
		}
		else console.error("Render not ready for currently active scene!!!");
	}

	public updateCameraMatrix(): void{
		for(let go=0; go<this.gameObjects.length; go++){
			this.gameObjects[go]?.updateCameraMatrix({
				projectionMatrix: this.activeCamera?.projectionMatrix,
				viewMatrix: this.activeCamera?.viewMatrix
			});
		}
	}

	public updateSceneLights(): void{
		const lightProps: ILightProps[] = this.generateLightProps();

		for(let go=0; go<this.gameObjects.length; go++){
			this.gameObjects[go]?.updateLightProps(lightProps);
		}
	}

	public dispose(): void{
		return;
	}

	public hasCompiled(): boolean{
		return this.renderReady;
	}
}

export {
	Scene
};