import { vec4 } from "gl-matrix";
import { Color, uuidv4 } from "./utilities";
import type { IUniform } from "./shader";
import type { IShaderTexts } from "./shader.data";
import { generateShaderTexts } from "./shader.data";
import type { ILightProps } from "./light";

interface IMaterialProps{
	id?: string;
	color?: Color | undefined;
	texture?: string | HTMLImageElement;
	unlit?: boolean;
	pointSize?: number;
	debug?: boolean;
	flat?: boolean;
}

class Material{

	public id: string;
	private materialProps: IMaterialProps;

	constructor(materialProps?: IMaterialProps){
		this.setupDefaults(materialProps);
	}

	private setupDefaults(materialProps?: IMaterialProps): void{
		const newUUID = uuidv4();
		const defaultMaterialProps = materialProps ?? {id: newUUID};
		if(!defaultMaterialProps.id) defaultMaterialProps.id = newUUID;
		this.id = defaultMaterialProps.id;

		if(!defaultMaterialProps.unlit) defaultMaterialProps.unlit = false;
		if(!defaultMaterialProps.color && !defaultMaterialProps.debug) defaultMaterialProps.color = new Color(1.0, 1.0, 1.0, 1.0);
		if(defaultMaterialProps.debug) defaultMaterialProps.color = undefined;

		this.materialProps = defaultMaterialProps;
	}

	private loadImage(imageString: string): Promise<HTMLImageElement>{
		const textureImage = new Image();
		textureImage.src = imageString;
		return new Promise((resolve, reject) => {
			textureImage.onload = (): void => {
				resolve(textureImage);
			};
			textureImage.onerror = (): void => {
				reject("Error loading image!!!");
			};
		});
	}

	public async loadAssets(): Promise<void>{
		try{
			if(typeof this.materialProps.texture === "string"){
				this.materialProps.texture = await this.loadImage(this.materialProps.texture);
			}
		}catch(err){
			console.error(err);
		}
	}

	public generateUniforms(): IUniform[]{
		const matUniforms: IUniform[] = [];

		if(this.materialProps.color) matUniforms.push({
			name: "u_color",
			data: vec4.fromValues(this.materialProps.color.r, this.materialProps.color.g, this.materialProps.color.b, this.materialProps.color.a)
		});
		if(this.materialProps.texture) matUniforms.push({
			name: "u_sampler",
			data: this.materialProps.texture as HTMLImageElement
		});

		return matUniforms;
	}

	public generateShaderTexts(lightProps: ILightProps[]): IShaderTexts{
		const shaderTexts = generateShaderTexts(this.materialProps, lightProps);
		return shaderTexts;
	}
}

export {
	Material,
	IMaterialProps
};