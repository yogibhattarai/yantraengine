import { ELightType } from "./light";
import type { ILightProps } from "./light";
import type { IMaterialProps } from "./material";
import { Color, Vector } from "./utilities";

interface IShaderTexts{
	vertex: string;
	fragment: string;
}

const generateVertexShaderTexts = (materialProps: IMaterialProps): string => {
	const GENERATED_VERTEX_SHADER = `
		#version 300 es

		uniform mat4 u_projectionMatrix;
		uniform mat4 u_viewMatrix;
		uniform mat4 u_modelMatrix;
		uniform mat4 u_normalMatrix;

		in vec3 a_position;
		in vec4 a_color;
		in vec2 a_uv;
		in vec3 a_normal;

		${(materialProps.flat?"flat ":"smooth ")}out vec4 v_color;
		out vec2 v_uv;

		out highp vec4 v_transformedNormal;

		void main(){
			v_color = a_color;
			v_uv = a_uv;

			gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * vec4(a_position, 1.0);
			gl_PointSize = ${materialProps?.pointSize?.toFixed(2) ?? "1.0"};

			v_transformedNormal = u_normalMatrix * vec4(a_normal, 1.0);
		}
	`;

	return GENERATED_VERTEX_SHADER;
};

const generateDirectionalLight = (ambientLightColor: Color, directionalLightColor: Color, directionalLightVector: Vector): string => {
	return `
		highp vec3 ambientLight = vec3(${ambientLightColor.r.toFixed(2)}, ${ambientLightColor.g.toFixed(2)}, ${ambientLightColor.b.toFixed(2)});
		highp vec3 directionalLightColor = vec3(${directionalLightColor.r.toFixed(2)}, ${directionalLightColor.g.toFixed(2)}, ${directionalLightColor.b.toFixed(2)});
		highp vec3 directionalVector = normalize(vec3(${directionalLightVector.x.toFixed(2)}, ${directionalLightVector.y.toFixed(2)}, ${directionalLightVector.z.toFixed(2)}));

		highp float directional = max(dot(v_transformedNormal.xyz, directionalVector), 0.0);
		vec3 directionalLight = ambientLight + (directionalLightColor * directional);
	`;
};

const generateFragmentShaderTexts = (materialProps: IMaterialProps, lightProps: ILightProps[]): string => {
	const GENERATED_UNLIT_FRAGMENT_SHADER = `
		#version 300 es

		precision highp float;

		${(materialProps.flat?"flat ":"smooth ")}in vec4 v_color;
		in vec2 v_uv;

		${materialProps?.color ? "uniform vec4 u_color;" : ""}
		${materialProps?.texture ? "uniform sampler2D u_sampler;" : ""}

		out vec4 fragColor;

		void main(){
			${materialProps?.color && materialProps.texture ? `
				vec4 prepped_texture = texture(u_sampler, v_uv);
				fragColor = prepped_texture * u_color;
			`:""}
			${materialProps?.color && !materialProps?.texture ? "fragColor = u_color;" : ""}
			${!materialProps?.color && materialProps?.texture ? "fragColor = texture(u_sampler, v_uv);" : ""}
			${!materialProps?.color && !materialProps.texture ? "fragColor = v_color;":""}
		}
	`;

	const ambientLight: ILightProps = lightProps.find(light=>light.type===ELightType.Ambient) as ILightProps;
	const directionalLight: ILightProps = lightProps.find(light=>light.type===ELightType.Directional) as ILightProps;

	let generatedDirectionalLightString = "";
	if(ambientLight && directionalLight) generatedDirectionalLightString = generateDirectionalLight(
		new Color(ambientLight.color?.r, ambientLight.color?.g, ambientLight.color?.b),
		new Color(directionalLight.color?.r, directionalLight.color?.g, directionalLight.color?.b),
		new Vector(directionalLight.direction?.x, directionalLight.direction?.y, directionalLight.direction?.z)
	);

	const GENERATED_LIT_FRAGMENT_SHADER = `
		#version 300 es

		precision highp float;

		${(materialProps.flat?"flat ":"smooth ")}in vec4 v_color;
		in vec2 v_uv;

		in vec4 v_transformedNormal;

		${materialProps?.color ? "uniform vec4 u_color;" : ""}
		${materialProps?.texture ? "uniform sampler2D u_sampler;" : ""}

		out vec4 fragColor;

		void main(){

			${generatedDirectionalLightString}

			${materialProps?.color && materialProps.texture ? `
				vec4 prepped_texture = texture(u_sampler, v_uv);
				vec4 texture_color_multiply = prepped_texture * u_color;
				fragColor = vec4(texture_color_multiply.rgb * directionalLight, texture_color_multiply.a);
			`:""}
			${materialProps?.color && !materialProps?.texture ? "fragColor = vec4(u_color.rgb * directionalLight, u_color.a);" : ""}
			${!materialProps?.color && materialProps?.texture ? `
				vec4 prepped_texture = texture(u_sampler, v_uv);
				fragColor = vec4(prepped_texture.rgb * directionalLight, prepped_texture.a);
			`: ""}
			${!materialProps?.color && !materialProps.texture ? `
				fragColor = vec4(v_color.rgb * directionalLight, v_color.a);
			`:""}
		}
	`;

	let FRAGSHADERTOUSE = GENERATED_LIT_FRAGMENT_SHADER;

	if(!ambientLight || !directionalLight || materialProps.unlit) FRAGSHADERTOUSE = GENERATED_UNLIT_FRAGMENT_SHADER;

	return FRAGSHADERTOUSE;
};

const generateShaderTexts = (materialProps: IMaterialProps, lightProps: ILightProps[]): IShaderTexts => {
	const generatedVertexShader = generateVertexShaderTexts(materialProps);
	const generatedFragmentShader = generateFragmentShaderTexts(materialProps, lightProps);

	return {
		vertex: generatedVertexShader,
		fragment: generatedFragmentShader
	};
};

export {
	generateShaderTexts,
	IShaderTexts
};