import type { vec3, vec4, mat4 } from "gl-matrix";
import type { IShaderTexts } from "./shader.data";

class Shader{
	public static gl: WebGL2RenderingContext;

	private shaderProgram: WebGLProgram;
	private shaderProps: IShaderObject;
	private vao: WebGLVertexArrayObject;

	private attachedShaders: ICompiledShaders;

	public compiled: boolean = false;

	constructor(){}

	public create(shaderProps: IShaderObject, shaderTexts: IShaderTexts): void{
		this.shaderProps = shaderProps;

		this.createShaderProgram(shaderTexts.vertex, shaderTexts.fragment);

		this.generateVAO();
		this.compiled = true;
	}

	public recreate(shaderTexts: IShaderTexts, shaderProps?: IShaderObject): void{
		this.compiled = false;

		this.createShaderProgram(shaderTexts.vertex, shaderTexts.fragment);

		if(shaderProps){
			this.shaderProps = shaderProps;
			this.generateVAO();
			this.compiled = true;
		}
	}

	private createShaderProgram(vertexShaderText: string, fragmentShaderText: string): void{
		if(this.shaderProgram && this.attachedShaders){
			Shader.gl.detachShader(this.shaderProgram, this.attachedShaders.vertex);
			Shader.gl.detachShader(this.shaderProgram, this.attachedShaders.fragment);
		}

		this.attachedShaders = this.compileShaders(vertexShaderText, fragmentShaderText);

		if(!this.shaderProgram) this.shaderProgram = Shader.gl.createProgram() as WebGLProgram;

		Shader.gl.attachShader(this.shaderProgram, this.attachedShaders.vertex);
		Shader.gl.attachShader(this.shaderProgram, this.attachedShaders.fragment);
		Shader.gl.linkProgram(this.shaderProgram);

		if(!Shader.gl.getProgramParameter(this.shaderProgram, Shader.gl.LINK_STATUS)){
			console.error(
				"Could not link shader program!!!",
				Shader.gl.getProgramInfoLog(this.shaderProgram)
			);
		}
	}

	private compileShaders(vertexShaderText: string, fragmentShaderText: string): ICompiledShaders{

		const vertexShader = Shader.gl.createShader(Shader.gl.VERTEX_SHADER) as WebGLShader;
		Shader.gl.shaderSource(vertexShader, vertexShaderText.trim());
		Shader.gl.compileShader(vertexShader);

		const fragmentShader = Shader.gl.createShader(Shader.gl.FRAGMENT_SHADER) as WebGLShader;
		Shader.gl.shaderSource(fragmentShader, fragmentShaderText.trim());
		Shader.gl.compileShader(fragmentShader);

		const vertexCompiled = Shader.gl.getShaderParameter(vertexShader, Shader.gl.COMPILE_STATUS) as boolean;
		const fragmentCompiled = Shader.gl.getShaderParameter(fragmentShader, Shader.gl.COMPILE_STATUS) as boolean;

		if(
			!vertexCompiled ||
			!fragmentCompiled
		){
			const errorMessage = !vertexCompiled ? Shader.gl.getShaderInfoLog(vertexShader) : Shader.gl.getShaderInfoLog(fragmentShader);
			console.error(
				"Could not compile shader(s)!!!",
				errorMessage
			);
		}

		return {
			vertex: vertexShader,
			fragment: fragmentShader
		};
	}

	private generateVAO(): void{
		Shader.gl.useProgram(this.shaderProgram);

		this.vao = Shader.gl.createVertexArray() as WebGLVertexArrayObject;
		Shader.gl.bindVertexArray(this.vao);

		if(this.shaderProps.attributes.length){
			for(let sa=0; sa<this.shaderProps.attributes.length; sa++){
				if(this.shaderProps.attributes[sa]){
					if(typeof this.shaderProps.attributes[sa].dataType === "number") this.shaderProps.attributes[sa].location = Shader.gl.getAttribLocation(this.shaderProgram, this.shaderProps.attributes[sa].name);
					this.shaderProps.attributes[sa].buffer = Shader.gl.createBuffer() as WebGLBuffer;
				}
			}
		}

		if(this.shaderProps.uniforms.length){
			for(let su=0; su<this.shaderProps.uniforms.length; su++){
				if(this.shaderProps.uniforms[su]){
					this.shaderProps.uniforms[su].location = Shader.gl.getUniformLocation(this.shaderProgram, this.shaderProps.uniforms[su].name) as WebGLUniformLocation;

					if(this.shaderProps.uniforms[su]?.data instanceof HTMLImageElement){
						this.shaderProps.uniforms[su].textureIndex = su;
						const textureIndex = `TEXTURE${this.shaderProps.uniforms[su].textureIndex ?? 0}`;
						Shader.gl.activeTexture(Shader.gl[textureIndex as keyof typeof Shader.gl] as number);
						this.shaderProps.uniforms[su].glTexture = Shader.gl.createTexture() as WebGLTexture;
						Shader.gl.bindTexture(Shader.gl.TEXTURE_2D, this.shaderProps.uniforms[su].glTexture as WebGLTexture);
						// Shader.gl.pixelStorei(Shader.gl.UNPACK_FLIP_Y_WEBGL, true);
						Shader.gl.texImage2D(Shader.gl.TEXTURE_2D, 0, Shader.gl.RGB, Shader.gl.RGB, Shader.gl.UNSIGNED_BYTE, this.shaderProps.uniforms[su].data as HTMLImageElement);

						Shader.gl.texParameteri(Shader.gl.TEXTURE_2D, Shader.gl.TEXTURE_MAG_FILTER, Shader.gl.LINEAR);      // this.gl.NEAREST
						Shader.gl.texParameteri(Shader.gl.TEXTURE_2D, Shader.gl.TEXTURE_MIN_FILTER, Shader.gl.LINEAR);
					}
				}
			}
		}
	}

	public useVAO(): void{
		Shader.gl.useProgram(this.shaderProgram);

		Shader.gl.bindVertexArray(this.vao);

		for(let sa=0; sa<this.shaderProps.attributes.length; sa++){
			if(typeof this.shaderProps.attributes[sa].dataType === "number"){
				Shader.gl.bindBuffer(Shader.gl.ARRAY_BUFFER, this.shaderProps.attributes[sa].buffer as WebGLBuffer);
				Shader.gl.bufferData(Shader.gl.ARRAY_BUFFER, this.shaderProps.attributes[sa].data, Shader.gl.DYNAMIC_DRAW);		// Shader.gl.STREAM_DRAW
			}
			else{
				Shader.gl.bindBuffer(Shader.gl.ELEMENT_ARRAY_BUFFER, this.shaderProps.attributes[sa].buffer as WebGLBuffer);
				Shader.gl.bufferData(Shader.gl.ELEMENT_ARRAY_BUFFER, this.shaderProps.attributes[sa].data, Shader.gl.DYNAMIC_DRAW);
			}
			if(typeof this.shaderProps.attributes[sa].dataType === "number"){
				Shader.gl.enableVertexAttribArray(this.shaderProps.attributes[sa].location as number);
				Shader.gl.vertexAttribPointer(this.shaderProps.attributes[sa].location as number, this.shaderProps.attributes[sa].dimension as number, this.shaderProps.attributes[sa].dataType as number, false, 0, 0);
			}
		}

		for(let su=0; su<this.shaderProps.uniforms.length; su++){
			if(this.shaderProps.uniforms[su]?.data instanceof HTMLImageElement)
				Shader.gl.uniform1i(this.shaderProps.uniforms[su].location as number, this.shaderProps.uniforms[su].textureIndex ?? 0);
			else if(
				!(this.shaderProps.uniforms[su]?.data instanceof HTMLImageElement) &&
				(this.shaderProps.uniforms[su]?.data as number[])?.length === 3
			)
				Shader.gl.uniform3fv(this.shaderProps.uniforms[su].location as number, this.shaderProps.uniforms[su]?.data as number[]);
			else if(
				!(this.shaderProps.uniforms[su]?.data instanceof HTMLImageElement) &&
				(this.shaderProps.uniforms[su]?.data as number[])?.length === 4
			)
				Shader.gl.uniform4fv(this.shaderProps.uniforms[su].location as number, this.shaderProps.uniforms[su]?.data as number[]);
			else if(
				!(this.shaderProps.uniforms[su]?.data instanceof HTMLImageElement) &&
				(this.shaderProps.uniforms[su]?.data as number[])?.length > 4
			)
				Shader.gl.uniformMatrix4fv(this.shaderProps.uniforms[su].location as number, false, this.shaderProps.uniforms[su].data as Float32Array);
		}
	}

	public updateShaderProps(shaderProps: IShaderObject): void{
		if(shaderProps?.attributes?.length){
			const attributesDiff = shaderProps.attributes.filter(attribute => !this.shaderProps.attributes.find(_attribute=>_attribute.name===attribute.name));
			if(attributesDiff?.length) throw new Error("Shader properties does not match inital properties!!!");

			for(let ac=0; ac<shaderProps.attributes.length; ac++){
				const _attribute = shaderProps?.attributes[ac];
				if(_attribute){
					const foundAttributeIndex = this.shaderProps.attributes.findIndex(attribute=>attribute.name===_attribute.name);
					if(!isNaN(foundAttributeIndex) && foundAttributeIndex>=0) this.shaderProps.attributes[foundAttributeIndex].data = _attribute.data;
				}
			}
		}

		// const uniformSymmDiff = shaderProps.uniforms
		// 	.filter(uniform => !this.shaderProps.uniforms.find(_uniform=>_uniform.name===uniform.name))
		// 	.concat(this.shaderProps.uniforms.filter(_uniform => !shaderProps.uniforms.find(uniform => uniform.name===_uniform.name)));
		// console.log(uniformSymmDiff);

		if(shaderProps?.uniforms?.length){
			const uniformDiff = shaderProps.uniforms.filter(uniform => !this.shaderProps.uniforms.find(_uniform=>_uniform.name===uniform.name));
			if(uniformDiff?.length) throw new Error("Shader properties does not match inital properties!!!");

			for(let uc=0; uc<shaderProps.uniforms.length; uc++){
				const _uniform = shaderProps?.uniforms[uc];
				if(_uniform){
					const foundUniformIndex = this.shaderProps.uniforms.findIndex(uniform=>uniform.name===_uniform.name);
					if(isNaN(foundUniformIndex) && foundUniformIndex>=0) this.shaderProps.uniforms[foundUniformIndex].data = _uniform.data;
				}
			}
		}
	}
}

interface IAttribute{
	name: string;
	dataType: number | string;
	data: Float32Array | Uint16Array;
	dimension?: number;
	buffer?: WebGLBuffer;
	location?: number;
}

interface IUniform{
	name: string;
	data: mat4 | vec3 | vec4 | HTMLImageElement;
	location?: WebGLUniformLocation;
	glTexture?: WebGLTexture;
	textureIndex?: number;
}

interface IShaderObject{
	attributes: IAttribute[];
	uniforms: IUniform[];
}

interface ICompiledShaders{
	vertex: WebGLShader;
	fragment: WebGLShader;
}

export {
	Shader,
	IShaderObject,
	IAttribute,
	IUniform
};