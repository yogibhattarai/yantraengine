const uuidv4 = (): string => {
	const _M = randomInt(6, 1);
	const _N = ["8", "9", "a", "b"][randomInt(3)];
	const placeholder = `xxxxxxxx-xxxx-${_M}xxx-${_N ?? ""}xxx-xxxxxxxxxxxx`;
	return placeholder.replace(/[xy]/g, (c)=>{
		const r = Math.random() * 16 | 0;
		const v = c == "x" ? r : (r & 0x3 | 0x8);
		return v.toString(16);
	});
};

const randomInt = (max: number, min: number = 0, seedString?: string): number => {
	if(seedString){
		const randfunc = mulberry32(seedString);
		const rand = randfunc();
		min = Math.ceil(min);
		max = Math.floor(max);
		return Math.floor(rand * (max - min) + min);
	}
	else{
		min = Math.ceil(min);
		max = Math.floor(max);
		return Math.floor(Math.random() * (max - min) + min);
	}
};

const randomFloat = (max: number = 1.0, min: number = 0.0): number => {
	return Math.random() * (max - min) + min;
};

const generateSeed = (seedString?: string): number => {
	if(!seedString){
		seedString = Math.random().toString(36).replace(/[^a-z]+/g, "").substring(0, 12);
	}
	const newseed = mulberry32(seedString);
	return newseed();
};

const mulberry32 = (_seed: string): ()=>number => {
	let seed: number = Number(_seed.split("").map(a=>a.charCodeAt(0)).join(""));
	return (): number => {
		seed |= 0;
		seed = seed + 0x6D2B79F5 | 0;
		let imul = Math.imul(seed ^ seed >>> 15, 1 | seed);
		imul = imul + Math.imul(imul ^ imul >>> 7, 61 | imul) ^ imul;
		return ((imul ^ imul >>> 14) >>> 0) / 4294967296;
	};
};

const easeOutQuad = (n: number): number => {
	return 1 - (1 - n) * (1 - n);
};

const easeOutBounce = (x: number): number => {
	const n1 = 7.5625;
	const d1 = 2.75;

	if(x < 1 / d1) return n1 * x * x;
	else if(x < 2 / d1) return n1 * (x -= 1.5 / d1) * x + 0.75;
	else if(x < 2.5 / d1) return n1 * (x -= 2.25 / d1) * x + 0.9375;
	else return n1 * (x -= 2.625 / d1) * x + 0.984375;
};

const toRadian = (x: number): number => {
	return x * Math.PI/180;
};

const toDegree = (x: number): number => {
	return x * 180/Math.PI;
};

const lerp = (n1: number, n2: number, alpha: number): number => {
	return (1 - alpha) * n1 + alpha * n2;
	// return a + (b - a) * alpha;
};

const clamp = (a: number, min: number = 0, max: number = 1): number => {
	return Math.min(max, Math.max(min, a));
};

const inverseLerp = (x: number, y: number, a: number): number => {
	return clamp((a - x) / (y - x));
};

const loadResource = (filepath: string, type: string = "text"): Promise<Blob | string> => {
	return new Promise((resolve, reject)=>{
		fetch(filepath, {
			method: "GET",
			mode: "no-cors"
		})
			.then(response=>{
				if(response.ok){
					if(type==="blob") resolve(response.blob());
					else resolve(response.text());
				}
				else reject("Invalid File");
			})
			.then(resData=>{/*console.log(resData);*/})
			.catch(error=>{
				console.log(error);
				reject(error);
			});
	});
};

const wait = (timeInMS: number): Promise<boolean> => {
	const startWaitTime: number = new Date().getTime();
	return new Promise((resolve, _reject)=>{
		setTimeout(()=>{
			resolve(true);

			const endWaitTime: number = new Date().getTime();
			console.log("Time Waited: ", endWaitTime-startWaitTime);
		}, timeInMS);
	});
};

const findFPS = (): Promise<number> => {
	return new Promise((resolve, _reject)=>{
		const fpslist: number[] = [];
		let count = 250;    // 250 frame sampled
		let then: number = 0;
		const FPSLoop = (now: DOMHighResTimeStamp): void => {
			if(count > 0) requestAnimationFrame(FPSLoop);
			else{
				const avgFPS = fpslist.reduce((a, b) => a + b) / fpslist.length;
				resolve(avgFPS);
			}

			now *= 0.001;
			const deltaTime = now - then;
			then = now;
			const fps = 1 / deltaTime;

			fpslist.push(fps);

			count--;
		};
		requestAnimationFrame(FPSLoop);
	});
};

class Vector{

	public id: string = uuidv4();
	
	public x: number = 0.0;
	public y: number = 0.0;
	public z: number = 0.0;
	public w: number = 0.0;

	constructor(x: number = 0.0, y: number = 0.0, z: number = 0.0, w: number = 0){
		this.x = x;
		this.y = y;
		if(z!==undefined && !isNaN(z)) this.z = z;
		if(w!==undefined && !isNaN(w)) this.w = w;
	}

	public clone(): Vector{
		return new Vector(this.x, this.y, this.z, this.w);
	}

	public get magnitude(): number{
		if(this.z) return Math.sqrt((this.x * this.x) + (this.y * this.y) + (this.z * this.z));
		else return Math.sqrt((this.x * this.x) + (this.y * this.y));
	}

	public divide(scalar: number = 1): Vector{
		if(this.z) return new Vector(this.x/scalar, this.y/scalar, this.z/scalar);
		else return new Vector(this.x/scalar, this.y/scalar);
	}

	public multiply(scalar:number = 1): Vector{
		if(this.z) return new Vector(this.x * scalar, this.y * scalar, this.z * scalar);
		else return new Vector(this.x * scalar, this.y * scalar);
	}

	public get normalize(): Vector{
		if(this.magnitude > 0.00001) return this.divide(this.magnitude);
		else return new Vector(0, 0, 0);
	}

	public cross(vectorB: Vector): Vector{
		if(this.z && vectorB.z){
			return new Vector(
				this.y * vectorB.z - this.z * vectorB.y,
				this.z * vectorB.x - this.x * vectorB.z,
				this.x * vectorB.y - this.y * vectorB.x
			);
		}
		else{
			return new Vector(vectorB.y, -vectorB.x);
		}
	}

	public substract(vectorB: Vector): Vector{
		if(this.z && vectorB.z) return new Vector(
			this.x - vectorB.x,
			this.y - vectorB.y,
			this.z - vectorB.z
		);
		else return new Vector(
			this.x - vectorB.x,
			this.y - vectorB.y
		);
	}
}

class Color{

	public id: string = uuidv4();

	public r: number = 1.0;
	public g: number = 1.0;
	public b: number = 1.0;
	public a: number = 1.0;

	constructor(r: number = 1.0, g: number = 1.0, b: number = 1.0, a: number = 1.0, format: string = "rgba"){
		const formatMatchingError: Error = new Error();
		formatMatchingError.name = "";
		if(
			format.length > 4 ||
			!/^[abgr]+$/.test(format.toLowerCase()) ||
			format.toLowerCase().split("").sort().join("").match(/(.)\1+/g)
		){
			formatMatchingError.name = "invalidformat";
			formatMatchingError.message = "Invalid color format";
		}

		if(formatMatchingError.name.length>0) throw formatMatchingError;
		else{
			const splitFormat = format.split("");

			const colorData: IColor = {
				r,
				g,
				b,
				a
			};

			for(let fc=0; fc<splitFormat.length; fc++){
				const colorName: string = (splitFormat?.length && splitFormat[fc]!==undefined ? splitFormat[fc] : "r");
				if(colorName==="r"){
					this.r = colorData[colorName] > 1.0 ? colorData[colorName]/256 : colorData[colorName];
				}
				else if(colorName==="g"){
					this.g = colorData[colorName] > 1.0 ? colorData[colorName]/256 : colorData[colorName];
				}
				else if(colorName==="b"){
					this.b = colorData[colorName] > 1.0 ? colorData[colorName]/256 : colorData[colorName];
				}
				else if(colorName==="a"){
					this.a = colorData[colorName] > 1.0 ? colorData[colorName]/256 : colorData[colorName];
				}
			}
		}
	}

	public clone(): Color{
		return new Color(this.r, this.g, this.b, this.a);
	}
}

interface IColor {
	r: number;
	g: number;
	b: number;
	a: number;
}

export {
	Vector,
	Color,

	uuidv4,
	randomInt,
	randomFloat,
	generateSeed,

	easeOutQuad,
	easeOutBounce,

	toRadian,
	toDegree,

	lerp,
	inverseLerp,
	clamp,

	loadResource,
	wait,
	findFPS
};