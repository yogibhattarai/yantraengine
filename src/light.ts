import { Color, Vector, uuidv4 } from "./utilities";
import type { Scene } from "./scene";

interface ILightProps{
	id?: string;
	type?: ELightType;
	position?: Vector;
	color?: Color;
	direction?: Vector;
}

class Light{
	
	public id: string = uuidv4();
	protected dlProps: ILightProps;

	protected parentScene: Scene;

	protected _position: Vector = new Vector(0, 2, 0);
	protected _color: Color = new Color(1, 1, 1);
	protected _direction: Vector = new Vector(0, 0, 0);

	constructor(dlProps?: ILightProps){
		this.setupDefaults(dlProps);

		this._position = this.dlProps.position as Vector;
		this._color = this.dlProps.color as Color;
		this._direction = this.dlProps.direction as Vector;
	}

	private setupDefaults(dlProps?: ILightProps): void{
		const newUUID = uuidv4();
		const defaultLightProps = dlProps ?? {id: newUUID};
		if(!defaultLightProps.id) defaultLightProps.id = newUUID;
		this.id = defaultLightProps.id;

		if(!defaultLightProps.position) defaultLightProps.position = new Vector(0, 2, 0);
		if(!defaultLightProps.color) defaultLightProps.color = new Color(1, 1, 1);
		if(!defaultLightProps.direction) defaultLightProps.direction = new Vector(0, 0, 0);

		this.dlProps = defaultLightProps;
	}

	public get position(): Vector{
		return this._position;
	}
	public set position(_position: Vector){
		this._position = _position.clone();
		this.updateSceneLights();
	}

	public get color(): Color{
		return this._color;
	}
	public set color(_color: Color){
		this._color = _color.clone();
		this.updateSceneLights();
	}

	public get direction(): Vector{
		return this._direction;
	}
	public set direction(_direction: Vector){
		this._direction = _direction.clone();
		this.updateSceneLights();
	}

	public get props(): ILightProps{
		return this.dlProps;
	}

	private updateSceneLights(): void{
		this.parentScene.updateSceneLights();
	}

	public setParentScene(scene: Scene): void{
		this.parentScene = scene;
	}
}

interface IALight{
	id?: string;
	color?: Color;
}

class AmbientLight extends Light{
	constructor(dlProps?: IALight){
		super(dlProps);
		this.dlProps.type = ELightType.Ambient;
	}
}

interface IDLight{
	id?: string;
	color?: Color;
	direction?: Vector;
}

class DirectionalLight extends Light{
	constructor(dlProps?: IDLight){
		super(dlProps);
		this.dlProps.type = ELightType.Directional;
	}
}

enum ELightType {
	Ambient = 0,
	Directional = 1,
	Point = 2
}

export {
	Light,
	DirectionalLight,
	AmbientLight,
	ELightType,
	ILightProps
};